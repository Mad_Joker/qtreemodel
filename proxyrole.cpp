#include "proxyrole.h"
#include "treeitem.h"
#include <QStringList>
#include <QDebug>
#include <QModelIndex>
#include <QQmlContext>
#include <QtQml>

#include "filter.h"
#include "treemodel.h"

ProxyItem::ProxyItem(QObject *parent) :
    QQmlPropertyMap(this, parent)
{

}


ProxyRole::ProxyRole(QObject *parent) :
    QObject(parent),
    m_name(QString()),
    m_ignore(false)
{

}

ProxyRole::~ProxyRole()
{

}

QString ProxyRole::name() const
{
    return m_name;
}

void ProxyRole::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged();
}

bool ProxyRole::ignore() const
{
    return m_ignore;
}

void ProxyRole::setIgnore(bool ignore)
{
    if (m_ignore == ignore)
        return;

    m_ignore = ignore;
    emit ignoreChanged();
}

void ProxyRole::invalidate()
{
    emit invalidated();
}


JoinRole::JoinRole(QObject *parent) :
    ProxyRole(parent),
    m_roles(QStringList()),
    m_separator(" ")
{

}

JoinRole::~JoinRole()
{

}

QStringList JoinRole::roles() const
{
    return m_roles;
}

void JoinRole::setRoles(QStringList roles)
{
    if (m_roles == roles)
        return;

    m_roles = roles;
    emit rolesChanged();
    invalidate();
}

QString JoinRole::separator() const
{
    return m_separator;
}

void JoinRole::setSeparator(QString separator)
{
    if (m_separator == separator)
        return;

    m_separator = separator;
    emit separatorChanged();
    invalidate();
}

void JoinRole::create(const QModelIndex &index)
{
    QString result;

    TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
    for (const QString &role : m_roles)
        result += item->value(role).toString() + m_separator;

    if (!m_roles.isEmpty())
        result.chop(m_separator.length());

    item->insert(name(), result);
}


FilterRole::FilterRole(QObject *parent) :
    ProxyRole(parent)
{

}

void FilterRole::create(const QModelIndex &index)
{
    bool accepted = std::all_of(m_filters.begin(), m_filters.end(), [=](Filter* filter) {
        return filter->filterRow(index);
    });
    static_cast<TreeItem *>(index.internalPointer())->insert(name(), accepted);
}

void FilterRole::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &FilterRole::invalidate);
    invalidate();
}


QueryRole::QueryRole(QObject *parent) :
    ProxyRole(parent),
    m_expression(nullptr),
    m_context(nullptr)
{
}

QQmlScriptString QueryRole::binding() const
{
    return m_scriptString;
}

void QueryRole::setBinding(const QQmlScriptString &scriptString)
{
    if (m_scriptString == scriptString)
        return;

    m_scriptString = scriptString;
    evaluate();

    emit bindingChanged();
    invalidate();
}

void QueryRole::create(const QModelIndex &index)
{
    QVariantList values;
    TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
    for (int i = 0 ; i < item->childCount(); ++i)
    {
        for (Filter *filter : m_filters)
        {
            if (!filter->enabled())
                continue;

            const QModelIndex childIndex = Collection::createIndexFromItem(i, item->child(i));
            if (filter->filterRow(childIndex) && !m_scriptString.isEmpty())
            {
                TreeItem *childItem = static_cast<TreeItem *>(childIndex.internalPointer());
                QQmlContext context(qmlContext(this));
                context.setContextProperty("model", childItem);

                QQmlExpression expression(m_scriptString, &context);
                values << evaluate(expression);
            }
        }
    }
    item->insert(name(), values);
}

void QueryRole::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &QueryRole::invalidate);
    invalidate();
}

void QueryRole::componentComplete()
{
    createContext();
}

void QueryRole::createContext()
{
    delete m_context;
    m_context = new QQmlContext(qmlContext(this), this);
    m_context->setContextProperty("model", nullptr);
    evaluate();
}

void QueryRole::evaluate()
{
    if (!m_context)
        return;

    delete m_expression;
    m_expression = new QQmlExpression(m_scriptString, m_context, this, this);
    connect(m_expression, &QQmlExpression::valueChanged, this, &ProxyRole::invalidated);
    m_expression->setNotifyOnValueChanged(true);
    m_expression->evaluate();
}

QVariant QueryRole::evaluate(QQmlExpression &expression) const
{
    QVariant result = expression.evaluate();
    if (expression.hasError())
    {
        qWarning() << expression.error();
        return QVariant();
    }

    return result;
}



ExpressionRole::ExpressionRole(QObject *parent) :
    ProxyRole(parent),
    m_expression(nullptr),
    m_context(nullptr)
{

}

QQmlScriptString ExpressionRole::expression() const
{
    return m_scriptString;
}

void ExpressionRole::setExpression(const QQmlScriptString &scriptString)
{
    if (m_scriptString == scriptString)
        return;

    m_scriptString = scriptString;
    evaluate();

    emit expressionChanged();
    invalidate();
}

void ExpressionRole::create(const QModelIndex &index)
{
    if (!m_scriptString.isEmpty())
    {
        TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
        QQmlContext context(qmlContext(this));
        context.setContextProperty("model", item);

        QQmlExpression expression(m_scriptString, &context);
        QVariant value = evaluate(expression);
        item->insert(name(), value);
    }
}

void ExpressionRole::componentComplete()
{
    createContext();
}

void ExpressionRole::createContext()
{
    delete m_context;
    m_context = new QQmlContext(qmlContext(this), this);
    m_context->setContextProperty("model", nullptr);
    evaluate();
}

void ExpressionRole::evaluate()
{
    if (!m_context)
        return;

    delete m_expression;
    m_expression = new QQmlExpression(m_scriptString, m_context, this, this);
    connect(m_expression, &QQmlExpression::valueChanged, this, &ProxyRole::invalidated);
    m_expression->setNotifyOnValueChanged(true);
    m_expression->evaluate();
}

QVariant ExpressionRole::evaluate(QQmlExpression &expression) const
{
    QVariant result = expression.evaluate();
    if (expression.hasError())
    {
        qWarning() << expression.error();
        return QVariant();
    }

    return result;
}
