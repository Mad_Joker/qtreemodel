#include "container.h"

Container::~Container() {}

QQmlListProperty<Filter> Container::filters()
{
    return QQmlListProperty<Filter>(reinterpret_cast<QObject *>(this),
                                    nullptr,
                                    &Container::append_filter,
                                    nullptr,
                                    nullptr,
                                    nullptr);
}

void Container::append_filter(QQmlListProperty<Filter> *list, Filter *filter)
{
    Container *container = reinterpret_cast<Container *>(list->object);
    container->m_filters.append(filter);
    container->onFilterAppended(filter);
}
