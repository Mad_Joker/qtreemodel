#ifndef QUERY_H
#define QUERY_H

#include <QObject>
#include <QVector>
#include <QVariant>
#include <QJSValue>

#include "treeitem.h"

class QueryItem : public TreeItem
{
    Q_OBJECT
public:
    explicit QueryItem(QObject *parent = nullptr) :
        TreeItem(parent)
    {
        setParentItem(nullptr);
    }

    bool isQueryItem() const
    {
        return true;
    }

    void setData(const QVector<TreeItem *> &children)
    {
        m_children = children;
    }
};

class Collection;
class QQmlEngine;
class Query : public QObject
{
    Q_OBJECT
public:
    explicit Query(QQmlEngine *engine, QObject *parent = nullptr);
    ~Query();

    inline QJSValue self() const { return m_self; }

public slots:
    QJSValue from(Collection *model);
    QJSValue select(const QString &role);
    QJSValue all(const QVariant &where);
    QJSValue any(const QVariant &where);
    QJSValue filter(QJSValue filter);
    int count();
    void commit();
    void revert();

private:
    void allOf(const QString &key, const QVariant &value, QVector<TreeItem *> &list, TreeItem *parent) const;
    bool filterRow(QJSValue fn, const QJSValueList &args) const;

private:
    QQmlEngine *m_engine;
    QJSValue m_self;
    Collection *m_model;
    QString m_role;

    QVector<TreeItem *> m_queryData;
};

#endif // QUERY_H
