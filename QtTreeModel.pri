!contains( CONFIG, c\+\+1[14] ): warning("SortFilterProxyModel needs at least c++11, add CONFIG += c++11 to your .pro")

INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/filter.cpp \
    $$PWD/query.cpp \
    $$PWD/sorter.cpp \
    $$PWD/container.cpp \
    $$PWD/section.cpp \
    $$PWD/proxyrole.cpp \
    $$PWD/parser.cpp \
    $$PWD/treeitem.cpp \
    $$PWD/treemodel.cpp

HEADERS += \
    $$PWD/filter.h \
    $$PWD/query.h \
    $$PWD/sorter.h \
    $$PWD/container.h \
    $$PWD/modelitem.h \
    $$PWD/section.h \
    $$PWD/proxyrole.h \
    $$PWD/parser.h \
    $$PWD/treeitem.h \
    $$PWD/treemodel.h
