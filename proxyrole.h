#ifndef PROXYROLE_H
#define PROXYROLE_H

#include <QObject>
#include <QQmlPropertyMap>
#include <QQmlScriptString>
#include <QQmlParserStatus>
#include "container.h"

class ProxyItem : public QQmlPropertyMap
{
    Q_OBJECT
public:
    explicit ProxyItem(QObject *parent = nullptr);

};

class ProxyRole : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool ignore READ ignore WRITE setIgnore NOTIFY ignoreChanged)

public:
    explicit ProxyRole(QObject *parent = nullptr);
    virtual ~ProxyRole();

    QString name() const;
    void setName(QString name);

    bool ignore() const;
    void setIgnore(bool ignore);

    virtual void create(const QModelIndex &index) = 0;

signals:
    void nameChanged();
    void ignoreChanged();
    void invalidated();

protected:
    void invalidate();

private:
    QString m_name;
    bool m_ignore;
};


class JoinRole : public ProxyRole
{
    Q_OBJECT
    Q_PROPERTY(QStringList roles READ roles WRITE setRoles NOTIFY rolesChanged)
    Q_PROPERTY(QString separator READ separator WRITE setSeparator NOTIFY separatorChanged)

public:
    explicit JoinRole(QObject *parent = nullptr);
    virtual ~JoinRole() override;

    QStringList roles() const;
    void setRoles(QStringList roles);

    QString separator() const;
    void setSeparator(QString separator);

    void create(const QModelIndex &index) override;

signals:
    void rolesChanged();
    void separatorChanged();

private:
    QStringList m_roles;
    QString m_separator;
};


class FilterRole : public ProxyRole, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit FilterRole(QObject *parent = nullptr);
    ~FilterRole() override {}

    void create(const QModelIndex &index) override;

signals:
    void filtersChanged();

private:
    void onFilterAppended(Filter *filter) override;
};

/*
 * QueryRole is used to query data that match filters from childrens
*/
class QQmlContext;
class QQmlExpression;
class QueryRole : public ProxyRole, public Container, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QQmlScriptString binding READ binding WRITE setBinding NOTIFY bindingChanged)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit QueryRole(QObject *parent = nullptr);
    ~QueryRole() override {}

    QQmlScriptString binding() const;
    void setBinding(const QQmlScriptString &scriptString);

    void create(const QModelIndex &index) override;

signals:
    void bindingChanged();
    void filtersChanged();

protected:
    void classBegin() override {}
    void componentComplete() override;

private:
    void onFilterAppended(Filter *filter) override;
    void createContext();
    void evaluate();
    QVariant evaluate(QQmlExpression &expression) const;

private:
    QQmlScriptString m_scriptString;
    QQmlExpression* m_expression;
    QQmlContext* m_context;
};


class ExpressionRole : public ProxyRole, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QQmlScriptString expression READ expression WRITE setExpression NOTIFY expressionChanged)

public:
    explicit ExpressionRole(QObject *parent = nullptr);
    ~ExpressionRole() override {}

    QQmlScriptString expression() const;
    void setExpression(const QQmlScriptString &scriptString);

    void create(const QModelIndex &index) override;

signals:
    void filtersChanged();
    void expressionChanged();

protected:
    void classBegin() override {}
    void componentComplete() override;

private:
    void createContext();
    void evaluate();
    QVariant evaluate(QQmlExpression &expression) const;

private:
    QQmlScriptString m_scriptString;
    QQmlExpression* m_expression;
    QQmlContext* m_context;
};


#endif // PROXYROLE_H
