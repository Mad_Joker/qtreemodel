#include "sorter.h"
#include "treeitem.h"
#include "filter.h"

#include <QDebug>
#include <QQmlContext>
#include <QQmlExpression>
#include <QtQml>
#include <qqmlinfo.h>

Sorter::Sorter(QObject *parent) :
    QObject(parent),
    m_enabled(true),
    m_sortOrder(Qt::AscendingOrder),
    m_ascendingOrder(m_sortOrder == Qt::AscendingOrder)
{
}

bool Sorter::enabled() const
{
    return m_enabled;
}

void Sorter::setEnabled(bool enabled)
{
    if (m_enabled == enabled)
        return;

    m_enabled = enabled;
    emit enabledChanged();
    emit invalidated();
}

Qt::SortOrder Sorter::sortOrder() const
{
    return m_sortOrder;
}

void Sorter::setSortOrder(Qt::SortOrder sortOrder)
{
    if (m_sortOrder == sortOrder)
        return;

    m_sortOrder = sortOrder;
    m_ascendingOrder = sortOrder == Qt::AscendingOrder;
    emit sortOrderChanged();
    invalidate();
}

bool Sorter::ascendingOrder() const
{
    return sortOrder() == Qt::AscendingOrder;
}

void Sorter::setAscendingOrder(bool ascendingOrder)
{
    setSortOrder(ascendingOrder ? Qt::AscendingOrder : Qt::DescendingOrder);
}

int Sorter::compareRows(const QModelIndex &left, const QModelIndex &right) const
{
    int comparison = compare(left, right);
    return (m_sortOrder == Qt::AscendingOrder) ? comparison : -comparison;
}

void Sorter::invalidate()
{
    if (enabled())
        emit invalidated();
}


RoleSorter::RoleSorter(QObject *parent) :
    Sorter(parent)
{

}

QString RoleSorter::role() const
{
    return m_role;
}

void RoleSorter::setRole(QString role)
{
    if (m_role == role)
        return;

    m_role = role;
    emit roleChanged();
    invalidate();
}

int RoleSorter::compare(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftValue = static_cast<TreeItem *>(left.internalPointer())->value(role());
    QVariant rightValue = static_cast<TreeItem *>(right.internalPointer())->value(role());

    if (leftValue < rightValue)
        return -1;
    if (leftValue > rightValue)
        return 1;
    return 0;
}


StringSorter::StringSorter(QObject *parent) :
    RoleSorter(parent)
{

}

Qt::CaseSensitivity StringSorter::caseSensitivity() const
{
    return m_collator.caseSensitivity();
}

void StringSorter::setCaseSensitivity(Qt::CaseSensitivity caseSensitivity)
{
    if (m_collator.caseSensitivity() == caseSensitivity)
        return;

    m_collator.setCaseSensitivity(caseSensitivity);
    emit caseSensitivityChanged();
    invalidate();
}

bool StringSorter::ignorePunctation() const
{
    return m_collator.ignorePunctuation();
}

void StringSorter::setIgnorePunctation(bool ignorePunctation)
{
    if (m_collator.ignorePunctuation() == ignorePunctation)
        return;

    m_collator.setIgnorePunctuation(ignorePunctation);
    emit ignorePunctationChanged();
    invalidate();
}

QLocale StringSorter::locale() const
{
    return m_collator.locale();
}

void StringSorter::setLocale(const QLocale &locale)
{
    if (m_collator.locale() == locale)
        return;

    m_collator.setLocale(locale);
    emit localeChanged();
    invalidate();
}

bool StringSorter::numericMode() const
{
    return m_collator.numericMode();
}

void StringSorter::setNumericMode(bool numericMode)
{
    if (m_collator.numericMode() == numericMode)
        return;

    m_collator.setNumericMode(numericMode);
    emit numericModeChanged();
    invalidate();
}

int StringSorter::compare(const QModelIndex &left, const QModelIndex &right) const
{
    QString leftValue = static_cast<TreeItem *>(left.internalPointer())->value(role()).toString();
    QString rightValue = static_cast<TreeItem *>(right.internalPointer())->value(role()).toString();
    return m_collator.compare(leftValue, rightValue);
}


FilterSorter::FilterSorter(QObject *parent) :
    Sorter(parent)
{
}

int FilterSorter::compare(const QModelIndex &left, const QModelIndex &right) const
{
    bool leftAccepted = filterAcceptsRow(left);
    bool rightAccepted = filterAcceptsRow(right);

    if (leftAccepted == rightAccepted)
        return 0;

    return leftAccepted ? -1 : 1;
}

void FilterSorter::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &FilterSorter::invalidate);
    invalidate();
}

bool FilterSorter::filterAcceptsRow(const QModelIndex &index) const
{
    return std::all_of(m_filters.begin(), m_filters.end(), [=](Filter *filter) {
        return filter->filterRow(index);
    });
}


ParentSorter::ParentSorter(QObject *parent) :
    RoleSorter(parent)
{
    setRole("name");
}

int ParentSorter::compare(const QModelIndex &left, const QModelIndex &right) const
{
    TreeItem *leftItem = static_cast<TreeItem *>(left.internalPointer());
    TreeItem *rightItem = static_cast<TreeItem *>(right.internalPointer());
    if (!leftItem->parentItem() || !rightItem->parentItem())
    {
        qmlInfo(this) << "Parent is null";
        return 0;
    }

    QVariant leftParent = leftItem->parentItem()->value(role());
    QVariant rightParent = rightItem->parentItem()->value(role());
    if (leftParent < rightParent)
        return -1;
    if (leftParent > rightParent)
        return 1;
    return 0;
}


ExpressionSorter::ExpressionSorter(QObject *parent) :
    Sorter(parent),
    m_expression(nullptr),
    m_context(nullptr)
{
}

QQmlScriptString ExpressionSorter::expression() const
{
    return m_scriptString;
}

void ExpressionSorter::setExpression(const QQmlScriptString &scriptString)
{
    if (m_scriptString == scriptString)
        return;

    m_scriptString = scriptString;
    evaluate();

    emit expressionChanged();
    invalidate();
}

void ExpressionSorter::componentComplete()
{
    createContext();
}

int ExpressionSorter::compare(const QModelIndex &left, const QModelIndex &right) const
{
    if (!m_scriptString.isEmpty())
    {
        QQmlContext context(qmlContext(this));
        QQmlExpression expression(m_scriptString, &context);

        TreeItem *leftItem = static_cast<TreeItem *>(left.internalPointer());
        TreeItem *rightItem = static_cast<TreeItem *>(right.internalPointer());

        context.setContextProperty("modelLeft", leftItem);
        context.setContextProperty("modelRight", rightItem);
        if (evaluate(expression))
                return -1;

        context.setContextProperty("modelLeft", rightItem);
        context.setContextProperty("modelRight", leftItem);
        if (evaluate(expression))
                return 1;
    }
    return 0;
}

void ExpressionSorter::createContext()
{
    delete m_context;
    m_context = new QQmlContext(qmlContext(this), this);

    m_context->setContextProperty("modelLeft", nullptr);
    m_context->setContextProperty("modelRight", nullptr);
    evaluate();
}

void ExpressionSorter::evaluate()
{
    if (!m_context)
        return;

    delete m_expression;
    m_expression = new QQmlExpression(m_scriptString, m_context, this, this);
    connect(m_expression, &QQmlExpression::valueChanged, this, &ExpressionSorter::invalidate);
    m_expression->setNotifyOnValueChanged(true);
    m_expression->evaluate();
}

bool ExpressionSorter::evaluate(QQmlExpression &expression) const
{
    QVariant result = expression.evaluate();
    if (expression.hasError())
    {
        qWarning() << expression.error();
        return false;
    }

    if (result.canConvert<bool>())
    {
        return result.toBool();
    }
    else
    {
        qWarning("%s:%i:%i : Can't convert result to bool",
                 expression.sourceFile().toUtf8().data(),
                 expression.lineNumber(),
                 expression.columnNumber());
        return false;
    }
}
