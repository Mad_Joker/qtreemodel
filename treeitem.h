#ifndef TREEITEM_H
#define TREEITEM_H

#include <QObject>
#include <QQmlPropertyMap>
#include <QVector>

class TreeItem : public QQmlPropertyMap
{
    Q_OBJECT
    Q_PROPERTY(uint iid READ iid WRITE setIid NOTIFY iidChanged)
    Q_PROPERTY(int level READ level WRITE setLevel NOTIFY levelChanged)
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString node READ node WRITE setNode NOTIFY nodeChanged)

public:
    explicit TreeItem(QObject *parent = nullptr);
    explicit TreeItem(TreeItem *parentItem, QObject *parent = nullptr);
    virtual ~TreeItem();

    uint iid() const;
    void setIid(uint iid);

    int level() const;
    void setLevel(const int level);
    void adjustLevels();

    QString node() const;
    void setNode(const QString &node);

    int type() const;
    void setType(int type);

    TreeItem *parentItem() const;
    void setParentItem(TreeItem *parent);
    void adjustParents();

    QVariant value(const QString &key) const;
    bool hasProperty(const QString &property) const;

    QVector<TreeItem *> children() const;
    TreeItem *child(int index) const;
    int childIndex(TreeItem *child);
    int index() const;
    bool isChildOf(uint iid) const;
    void appendChild(TreeItem *child);
    void removeChild(int index);
    bool hasChild(TreeItem *child) const;
    bool hasChildren() const;
    int childCount() const;

    static uint uniqueIid();
    void dump(TreeItem *item = nullptr, QString tab = QString());

    virtual bool isQueryItem() const { return false; }

signals:
    void iidChanged();
    void levelChanged();
    void typeChanged();
    void nodeChanged();

private:
    int m_level = 0;
    uint m_iid = 0;
    int m_type = 0;

    TreeItem *m_parentItem;

protected:
    QVector<TreeItem *> m_children;
};


class Parser;
class Tree : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(Parser* parser READ parser WRITE setParser NOTIFY parserChanged)

public:
    explicit Tree(QObject *parent = nullptr);
    virtual ~Tree();

    static Tree &instance(QObject *parent = nullptr);

    TreeItem *rootItem() const;

    QString source() const;
    void setSource(QString source);

    Parser *parser() const;
    void setParser(Parser *parser);

    void load(const QByteArray &serializedString);
    QByteArray serialize(TreeItem *parent = nullptr) const;
    TreeItem *find(const QString &name, TreeItem *parent = nullptr);
    TreeItem *find(uint iid, TreeItem *parent = nullptr);
    TreeItem *find(const QString &property, const QVariant &value, TreeItem *parent = nullptr) const;

    bool insert(uint parentIid, TreeItem *child);
    void insert(const QVariant &parentId, TreeItem *child);
    void insert(const QByteArray &serialized);
    void remove(const QVariant &id);
    bool remove(uint iid);

    Q_INVOKABLE QVariant item(const QString key, const QVariant &value) const;
    Q_INVOKABLE QVariant children(const QString key, const QVariant &value) const;
    Q_INVOKABLE void update(/*const QString &id, */const QByteArray &attributes);
    void dump();

    Q_INVOKABLE void test();

signals:
    void hintChanged();
    void sourceChanged();
    void parserChanged();

    void updated();
    void inserted(uint parent, uint iid);
    void removed(uint parent, uint iid);

    void loaded();
    void error(QString errorString, int offset);

private:
    TreeItem *find_helper(uint iid, TreeItem *parent) const;
    TreeItem *find_helper(const QString &name, TreeItem *parent) const;
    TreeItem *find_helper(const QString &key, const QVariant &value, TreeItem *parent) const;

private:
    TreeItem *m_rootItem;

    QString m_source;
    Parser* m_parser;
};
Q_DECLARE_METATYPE(TreeItem *)

#endif // TREEITEM_H
