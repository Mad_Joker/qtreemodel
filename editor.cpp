#include "editor.h"
#include "treeitem.h"
#include "parser.h"

#include <QDebug>

Editor::Editor(QObject *parent) :
    QObject(parent),
    m_rootItemIid(0),
    m_tree(nullptr)
{
}

QString Editor::node() const
{
    return m_node;
}

void Editor::setNode(QString node)
{
    if (m_node == node)
        return;

    m_node = node;
    emit nodeChanged();
}

void Editor::setTree(Tree *tree)
{
    if (m_tree == tree)
        return;

    m_tree = tree;
}

void Editor::setRootItemIid(uint iid)
{
    m_rootItemIid = iid;
}

uint Editor::create(const QVariantMap &properties)
{
    if (m_node.isEmpty())
    {
        qWarning() << "node are empty";
        return 0;
    }

    if (m_rootItemIid != 0 && m_tree)
    {
        TreeItem *child = new TreeItem;
        for (const QString &key : properties.keys())
            child->insert(key, properties.value(key));

        child->setType(Parser::Array);
        child->setNode(m_node);
        if (m_tree->insert(m_rootItemIid, child))
        {
            emit itemCreated(child->iid());
            return child->iid();
        }
    }
    return 0;
}

void Editor::remove(uint iid)
{
    if (m_tree->remove(iid))
        emit itemDeleted(iid);
}

void Editor::remove(const QVariant &item)
{
    if (!item.isValid())
        return;

    if (item.canConvert<uint>())
    {
        uint iid = item.toUInt();
        remove(iid);
    }
    else if (item.canConvert<TreeItem *>())
    {
        TreeItem *treeItem = item.value<TreeItem *>();
        remove(treeItem->iid());
    }
}

QByteArray Editor::serialize(const QVariant &parent) const
{
    if (!m_tree && !m_tree->parser())
    {
        qWarning() << "tree.parser are null";
        return QByteArray();
    }

    if (!parent.isValid())
        return QByteArray();

    if (parent.canConvert<uint>())
    {
        uint iid = parent.toUInt();
        return m_tree->parser()->serialize(m_tree->find(iid));
    }
    else if (parent.canConvert<TreeItem *>())
    {
        TreeItem *treeItem = parent.value<TreeItem *>();
        qDebug() << m_tree->parser()->serialize(treeItem);
        return m_tree->parser()->serialize(treeItem);
    }
    return QByteArray();
}

#define URI "com.qt.tree"
#include <QGuiApplication>
#include <QtQml>
static void registerType()
{
    qmlRegisterType<Editor>(URI, 1, 0, "Editor");
}
Q_COREAPP_STARTUP_FUNCTION(registerType)
