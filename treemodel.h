#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QQmlListProperty>
#include <QQmlParserStatus>
#include <QVector>

struct Mapping
{
    int rowCount = 0;
    QVector<QModelIndex> indices;

    void map(/*int row, */const QModelIndex &index)
    {
        indices << index;
    }
    QModelIndex convert(const QModelIndex &index) const
    {
        return indices[index.row()];
    }
    void clear()
    {
        indices.clear();
    }
    bool contains(const QModelIndex &index) const
    {
        return indices.contains(index);
    }
};

class Tree;
class TreeItem;

class Filter;
class Sorter;
class Section;
class ProxyRole;
class Collection : public QAbstractListModel,
                   public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int depth READ depth NOTIFY depthChanged)
    Q_PROPERTY(QVariant rootItem READ rootItem WRITE setRootItem NOTIFY rootItemChanged)
    Q_PROPERTY(Tree* tree READ tree WRITE setTree NOTIFY treeChanged)
    Q_PROPERTY(Section* section READ section WRITE setSection NOTIFY sectionChanged)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters)
    Q_PROPERTY(QQmlListProperty<Sorter> sorters READ sorters)
    Q_PROPERTY(QQmlListProperty<ProxyRole> proxyRoles READ proxyRoles)

public:
    enum Roles {
        ItemRole = Qt::UserRole + 1,
        SectionRole
    };

public:
    explicit Collection(QObject *parent = nullptr);
    ~Collection() override;

    int count() const;
    int depth() const;

    Tree *tree() const;
    void setTree(Tree* tree);

    Section *section() const;
    void setSection(Section *section);

    QVariant rootItem() const;
    void setRootItem(const QVariant &rootItem);
    void setRootItem(TreeItem *rootItem);
    void setRowCount(int count);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    static QModelIndex createIndexFromItem(int index, TreeItem *item);

    QQmlListProperty<Filter> filters();
    QQmlListProperty<Sorter> sorters();
    QQmlListProperty<ProxyRole> proxyRoles();

public slots:
    uint createItem(const QString &node, const QVariantMap &properties);
    void deleteItem(uint iid);
    void deleteItem(const QVariant &item);

signals:
    void countChanged();
    void depthChanged();
    void rootItemChanged();
    void treeChanged();
    void sectionChanged();

    void inserted();

    void itemCreated(uint iid);
    void itemDeleted(uint iid);

private:
    void mapToSource(/*int row, */const QModelIndex &index);
    QModelIndex mapFromSource(const QModelIndex &index) const;
    QModelIndex childIndex(int index, TreeItem *item) const;

    bool filterAcceptsRow(const QModelIndex &index) const;
    void invalidateSorters();
    void invalidateFilters();
    void createProxyRole(const QModelIndex &index);
    void invalidateProxyRoles();

    void insert(int size);
    void erase();
    void reset();
    void update();

    //handle tree changes
    void onItemInserted(uint parentIid, uint);
    void onItemDeleted(uint parentIid, uint iid);

    static void append_filter(QQmlListProperty<Filter> *list, Filter *filter);
    static void append_sorter(QQmlListProperty<Sorter> *list, Sorter *sorter);
    static void append_proxyRole(QQmlListProperty<ProxyRole> *list, ProxyRole *proxyRole);

protected:
    void classBegin() override {}
    void componentComplete() override {}
    QHash<int, QByteArray> roleNames() const override;

private:
    TreeItem *m_rootItem_ptr;
    Mapping m_mapping;

    int m_filteredRows = 0;

    QVariant m_rootItem;
    Tree *m_tree;
    Section *m_section;
    QList<Filter *> m_filters;
    QList<Sorter *> m_sorters;
    QList<ProxyRole *> m_proxyRoles;
};

#endif // TREEMODEL_H
