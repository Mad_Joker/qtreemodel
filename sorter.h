#ifndef SORTER_H
#define SORTER_H

#include <QObject>
#include <QVariant>
#include <QCollator>
#include <QQmlScriptString>
#include <QQmlParserStatus>
#include "container.h"

class Sorter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder NOTIFY sortOrderChanged)
    Q_PROPERTY(bool ascendingOrder READ ascendingOrder WRITE setAscendingOrder NOTIFY sortOrderChanged)

public:
    explicit Sorter(QObject *parent = nullptr);
    virtual ~Sorter() {}

    bool enabled() const;
    void setEnabled(bool enabled);

    Qt::SortOrder sortOrder() const;
    void setSortOrder(Qt::SortOrder sortOrder);

    bool ascendingOrder() const;
    void setAscendingOrder(bool ascendingOrder);

    virtual int compareRows(const QModelIndex &left, const QModelIndex &right) const;

signals:
    void enabledChanged();
    void sortOrderChanged();
    void invalidated();

protected:
    void invalidate();
    virtual int compare(const QModelIndex &left, const QModelIndex &right) const = 0;

private:
    bool m_enabled;
    Qt::SortOrder m_sortOrder;
    bool m_ascendingOrder;
};

class RoleSorter : public Sorter
{
    Q_OBJECT
    Q_PROPERTY(QString role READ role WRITE setRole NOTIFY roleChanged)

public:
    explicit RoleSorter(QObject *parent = nullptr);
    virtual ~RoleSorter() override {}

    QString role() const;
    void setRole(QString role);

signals:
    void roleChanged();

protected:
    int compare(const QModelIndex &left, const QModelIndex &right) const override;

private:
    QString m_role;
};

class StringSorter : public RoleSorter
{
    Q_OBJECT
    Q_PROPERTY(Qt::CaseSensitivity caseSensitivity READ caseSensitivity WRITE setCaseSensitivity NOTIFY caseSensitivityChanged)
    Q_PROPERTY(bool ignorePunctation READ ignorePunctation WRITE setIgnorePunctation NOTIFY ignorePunctationChanged)
    Q_PROPERTY(QLocale locale READ locale WRITE setLocale NOTIFY localeChanged)
    Q_PROPERTY(bool numericMode READ numericMode WRITE setNumericMode NOTIFY numericModeChanged)

public:
    explicit StringSorter(QObject *parent = nullptr);
    virtual ~StringSorter() override {}

    Qt::CaseSensitivity caseSensitivity() const;
    void setCaseSensitivity(Qt::CaseSensitivity caseSensitivity);

    bool ignorePunctation() const;
    void setIgnorePunctation(bool ignorePunctation);

    QLocale locale() const;
    void setLocale(const QLocale &locale);

    bool numericMode() const;
    void setNumericMode(bool numericMode);

Q_SIGNALS:
    void caseSensitivityChanged();
    void ignorePunctationChanged();
    void localeChanged();
    void numericModeChanged();

protected:
    int compare(const QModelIndex &left, const QModelIndex &right) const override;

private:
    QCollator m_collator;
};

class FilterSorter : public Sorter, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit FilterSorter(QObject *parent = nullptr);
    ~FilterSorter() override {}

signals:
    void filtersChanged();

protected:
    int compare(const QModelIndex &left, const QModelIndex &right) const override;

private:
    void onFilterAppended(Filter *filter) override;
    bool filterAcceptsRow(const QModelIndex &index) const;
};

class ParentSorter : public RoleSorter
{
    Q_OBJECT
public:
    explicit ParentSorter(QObject *parent = nullptr);
    ~ParentSorter() override {}

protected:
    int compare(const QModelIndex &left, const QModelIndex &right) const override;
};

class QQmlContext;
class QQmlExpression;
class ExpressionSorter : public Sorter, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QQmlScriptString expression READ expression WRITE setExpression NOTIFY expressionChanged)

public:
    explicit ExpressionSorter(QObject *parent = nullptr);
    ~ExpressionSorter() override {}

    QQmlScriptString expression() const;
    void setExpression(const QQmlScriptString &scriptString);

signals:
    void expressionChanged();

protected:
    void classBegin() override {}
    void componentComplete() override;
    int compare(const QModelIndex &left, const QModelIndex &right) const override;

private:
    void createContext();
    void evaluate();
    bool evaluate(QQmlExpression &expression) const;

private:
    QQmlScriptString m_scriptString;
    QQmlExpression* m_expression;
    QQmlContext* m_context;
};

#endif // SORTER_H
