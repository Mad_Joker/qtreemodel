#ifndef EDITOR_H
#define EDITOR_H

#include <QObject>
#include <QVariant>

class Tree;
class TreeItem;
class Editor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString node READ node WRITE setNode NOTIFY nodeChanged)

public:
    explicit Editor(QObject *parent = nullptr);
    ~Editor() {}

    QString node() const;
    void setNode(QString node);

    void setTree(Tree *tree);
    void setRootItemIid(uint iid);

signals:
    void nodeChanged();

    void itemCreated(uint iid);
    void itemDeleted(uint iid);

public slots:
    uint create(const QVariantMap &properties);
    void remove(uint iid);
    void remove(const QVariant &item);
    QByteArray serialize(const QVariant &parent) const;

private:
    QString m_node;
    uint m_rootItemIid;
    Tree *m_tree;
};

#endif // EDITOR_H
