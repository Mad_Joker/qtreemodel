#include "filter.h"
#include "treeitem.h"

#include <QQmlContext>
#include <QQmlExpression>
#include <QtQml>

Filter::Filter(QObject *parent) :
    QObject(parent),
    m_enabled(true)
{

}

bool Filter::enabled() const
{
    return m_enabled;
}

void Filter::setEnabled(bool enabled)
{
    if (m_enabled == enabled)
        return;

    m_enabled = enabled;
    emit enabledChanged();
    emit invalidated();
}

void Filter::invalidate()
{
    if (enabled())
        emit invalidated();
}

void Filter::refilter()
{
    invalidate();
}


IndexFilter::IndexFilter(QObject *parent) :
    Filter(parent)
{
}

qint32 IndexFilter::minimum() const
{
    return m_minimum;
}

void IndexFilter::setMinimum(qint32 minimum)
{
    if (m_minimum == minimum)
        return;

    m_minimum = minimum;
    emit minimumChanged();
    invalidate();
}

qint32 IndexFilter::maximum() const
{
    return m_maximum;
}

void IndexFilter::setMaximum(qint32 maximum)
{
    if (m_maximum == maximum)
        return;

    m_maximum = maximum;
    emit maximumChanged();
    invalidate();
}

bool IndexFilter::filterRow(const QModelIndex &index) const
{
    return !enabled() || (index.row() >= minimum() && index.row() <= maximum());
}


RoleFilter::RoleFilter(QObject *parent) :
    Filter(parent),
    m_role(QString())
{
}

QString RoleFilter::role() const
{
    return m_role;
}

void RoleFilter::setRole(const QString &role)
{
    if (m_role == role)
        return;

    m_role = role;
    emit roleChanged();
    invalidate();
}


ValueFilter::ValueFilter(QObject *parent) :
    RoleFilter(parent),
    m_value(QVariant())
{

}

QVariant ValueFilter::value() const
{
    return m_value;
}

void ValueFilter::setValue(const QVariant &value)
{
    if (m_value == value)
        return;

    m_value = value;
    emit valueChanged();
    invalidate();
}

bool ValueFilter::filterRow(const QModelIndex &index) const
{
    TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
    return !enabled() || item->value(role()) == value();
}


RangeFilter::RangeFilter(QObject *parent) :
    RoleFilter(parent),
    m_minimumValue(QVariant()),
    m_minimumInclusive(true),
    m_maximumValue(QVariant()),
    m_maximumInclusive(true)
{
}

QVariant RangeFilter::minimumValue() const
{
    return m_minimumValue;
}

void RangeFilter::setMinimumValue(const QVariant &minimumValue)
{
    if (m_minimumValue == minimumValue)
        return;

    m_minimumValue = minimumValue;
    emit minimumValueChanged();
}

bool RangeFilter::minimumInclusive() const
{
    return m_minimumInclusive;
}

void RangeFilter::setMinimumInclusive(bool minimumInclusive)
{
    if (m_minimumInclusive == minimumInclusive)
        return;

    m_minimumInclusive = minimumInclusive;
    emit minimumInclusiveChanged();
    invalidate();
}

QVariant RangeFilter::maximumValue() const
{
    return m_maximumValue;
}

void RangeFilter::setMaximumValue(const QVariant &maximumValue)
{
    if (m_maximumValue == maximumValue)
        return;

    m_maximumValue = maximumValue;
    emit maximumValueChanged();
    invalidate();
}

bool RangeFilter::maximumInclusive() const
{
    return m_maximumInclusive;
}

void RangeFilter::setMaximumInclusive(bool maximumInclusive)
{
    if (m_maximumInclusive == maximumInclusive)
        return;

    m_maximumInclusive = maximumInclusive;
    emit maximumInclusiveChanged();
    invalidate();
}

bool RangeFilter::filterRow(const QModelIndex &index) const
{
    QVariant value = static_cast<TreeItem *>(index.internalPointer())->value(role());
    bool lessThanMin = m_minimumValue.isValid() &&
            (m_minimumInclusive ? value < m_minimumValue : value <= m_minimumValue);
    bool moreThanMax = m_maximumValue.isValid() &&
            (m_maximumInclusive ? value > m_maximumValue : value >= m_maximumValue);
    return !enabled() || !(lessThanMin || moreThanMax);
}


RegExpFilter::RegExpFilter(QObject *parent) :
    RoleFilter(parent)
{
}

QString RegExpFilter::pattern() const
{
    return m_regExp.pattern();
}

void RegExpFilter::setPattern(const QString &pattern)
{
    if (m_regExp.pattern() == pattern)
        return;

    m_regExp.setPattern(pattern);
    emit patternChanged();
    invalidate();
}

RegExpFilter::PatternSyntax RegExpFilter::syntax() const
{
    return static_cast<PatternSyntax>(m_regExp.patternSyntax());
}

void RegExpFilter::setSyntax(RegExpFilter::PatternSyntax syntax)
{
    if (static_cast<PatternSyntax>(m_regExp.patternSyntax()) == syntax)
        return;

    m_regExp.setPatternSyntax(static_cast<QRegExp::PatternSyntax>(syntax));
    emit syntaxChanged();
    invalidate();
}

Qt::CaseSensitivity RegExpFilter::caseSensitivity() const
{
    return m_regExp.caseSensitivity();
}

void RegExpFilter::setCaseSensitivity(Qt::CaseSensitivity caseSensitivity)
{
    if (m_regExp.caseSensitivity() == caseSensitivity)
        return;

    m_regExp.setCaseSensitivity(caseSensitivity);
    emit caseSensitivityChanged();
    invalidate();
}

bool RegExpFilter::filterRow(const QModelIndex &index) const
{
    QString string = static_cast<TreeItem *>(index.internalPointer())->value(role()).toString();
    return !enabled() || m_regExp.indexIn(string) != -1;
}


AnyOf::AnyOf(QObject *parent) :
    Filter(parent)
{
}

bool AnyOf::filterRow(const QModelIndex &index) const
{
    return !enabled() || std::any_of(m_filters.begin(), m_filters.end(), [=](Filter *filter) {
        return filter->filterRow(index);
    });
}

void AnyOf::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &AnyOf::invalidate);
    invalidate();
}


AllOf::AllOf(QObject *parent) :
    Filter(parent)
{
}

bool AllOf::filterRow(const QModelIndex &index) const
{
    return !enabled() || std::all_of(m_filters.begin(), m_filters.end(), [=](Filter *filter) {
        return filter->filterRow(index);
    });
}

void AllOf::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &AllOf::invalidate);
    invalidate();
}


ExpressionFilter::ExpressionFilter(QObject *parent) :
    Filter(parent),
    m_expression(nullptr),
    m_context(nullptr)
{
}

QQmlScriptString ExpressionFilter::expression() const
{
    return m_scriptString;
}

void ExpressionFilter::setExpression(const QQmlScriptString &scriptString)
{
    if (m_scriptString == scriptString)
        return;

    m_scriptString = scriptString;
    evaluate();

    emit expressionChanged();
    invalidate();
}

bool ExpressionFilter::filterRow(const QModelIndex &index) const
{
    if (!m_scriptString.isEmpty())
    {
        QQmlContext context(qmlContext(this));
        context.setContextProperty("index", index.row());
        context.setContextProperty("model", static_cast<TreeItem *>(index.internalPointer()));

        QQmlExpression expression(m_scriptString, &context);
        return !enabled() || evaluate(expression);
    }
    return true;
}

void ExpressionFilter::componentComplete()
{
    createContext();
}

void ExpressionFilter::createContext()
{
    delete m_context;
    m_context = new QQmlContext(qmlContext(this), this);

    m_context->setContextProperty("index", -1);
    m_context->setContextProperty("model", nullptr);
    evaluate();
}

void ExpressionFilter::evaluate()
{
    if (!m_context)
        return;

    delete m_expression;
    m_expression = new QQmlExpression(m_scriptString, m_context, nullptr, this);
    connect(m_expression, &QQmlExpression::valueChanged, this, &ExpressionFilter::invalidate);
    m_expression->setNotifyOnValueChanged(true);
    m_expression->evaluate();
}

bool ExpressionFilter::evaluate(QQmlExpression &expression) const
{
    QVariant result = expression.evaluate();
    if (expression.hasError())
    {
        qWarning() << expression.error();
        return false;
    }

    if (result.canConvert<bool>())
    {
        return result.toBool();
    }
    else
    {
        qWarning("%s:%i:%i : Can't convert result to bool",
                 expression.sourceFile().toUtf8().data(),
                 expression.lineNumber(),
                 expression.columnNumber());
        return false;
    }
}

#include "treemodel.h"

ChildrenFilter::ChildrenFilter(QObject *parent) :
    Filter(parent)
{
}

bool ChildrenFilter::filterRow(const QModelIndex &index) const
{
    TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
    for (int i = 0 ; i < item->childCount(); ++i)
    {
        if (filterAcceptsRow(Collection::createIndexFromItem(i, item->child(i))))
            return true;
    }
    return !enabled();
}

bool ChildrenFilter::filterAcceptsRow(const QModelIndex &index) const
{
    return std::all_of(m_filters.begin(), m_filters.end(), [=](Filter *filter) {
        return filter->filterRow(index);
    });
}

void ChildrenFilter::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &ChildrenFilter::invalidate);
    invalidate();
}


Contains::Contains(QObject *parent) :
    RoleFilter(parent)
{
}

bool Contains::filterRow(const QModelIndex &index) const
{
    QVector<TreeItem *> tmpItems;
    TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
    if (!item->value(role()).canConvert<QVariantList>())
    {
        qWarning() << "Contains.role must be a list";
        return true;
    }

    QVariantList values = item->value(role()).toList();
    for (int i = 0; i < values.size(); ++i)
    {
        TreeItem *tmp = new TreeItem;
        tmp->insert(role(), values[i]);
        tmpItems << tmp;
        const QModelIndex tmpIndex = Collection::createIndexFromItem(i, tmp);

        if (filterAcceptsRow(tmpIndex))
            return true;
    }

    qDeleteAll(tmpItems);
    tmpItems.clear();
    return !enabled() || m_filters.isEmpty();
}

bool Contains::filterAcceptsRow(const QModelIndex &index) const
{
    return std::all_of(m_filters.begin(), m_filters.end(), [=](Filter *filter) {
        return filter->filterRow(index);
    });
}

void Contains::onFilterAppended(Filter *filter)
{
    if (RoleFilter *roleFilter = qobject_cast<RoleFilter *>(filter))
        roleFilter->setRole(role());

    connect(filter, &Filter::invalidated, this, &Contains::invalidate);
    invalidate();
}
