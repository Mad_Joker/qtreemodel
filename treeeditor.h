#ifndef TREEEDITOR_H
#define TREEEDITOR_H

#include <QObject>

class ItemData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString node READ node WRITE setNode NOTIFY nodeChanged)
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)

public:
    explicit ItemData(QObject *parent = nullptr);

    QString node() const;
    void setNode(QString node);

    int type() const;
    void setType(int type);

signals:
    void nodeChanged();
    void typeChanged();

private:
    QString m_node;
    int m_type;
};

class Tree;
class TreeItem;
class Parser;
class TreeEditor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint rootItemIid READ rootItemIid WRITE setRootItemIid NOTIFY rootItemIidChanged)
    Q_PROPERTY(Tree* tree READ tree WRITE setTree NOTIFY treeChanged)
    Q_PROPERTY(Parser* parser READ parser WRITE setParser NOTIFY parserChanged)
    Q_PROPERTY(ItemData* itemData READ itemData NOTIFY itemDataChanged)

public:
    explicit TreeEditor(QObject *parent = nullptr);

    uint rootItemIid() const;
    void setRootItemIid(uint rootItemIid);

    Tree *tree() const;
    void setTree(Tree* tree);

    Parser *parser() const;
    void setParser(Parser* parser);

    ItemData *itemData() const;

public slots:
    uint create(const QVariantMap &properties);
    void remove(uint iid);
    QByteArray serialize() const;

signals:
    void treeChanged();
    void parserChanged();
    void itemDataChanged();
    void rootItemIidChanged();
    void updated();

private:
    bool isTreeExist() const;
    void setRootItem(uint iid);

private:
    uint m_rootItemIid;
    Tree *m_tree;
    Parser *m_parser;
    TreeItem *m_rootItem;
    ItemData *m_itemData;
};

#define URI "com.qt.tree"
#include <QGuiApplication>
#include <QtQml>
static void registerType()
{
    qmlRegisterType<ItemData>(URI, 1, 0, "ItemData");
    qmlRegisterType<TreeEditor>(URI, 1, 0, "TreeEditor");
}
Q_COREAPP_STARTUP_FUNCTION(registerType)

#endif // TREEEDITOR_H
