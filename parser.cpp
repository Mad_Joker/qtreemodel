#include "parser.h"
#include "treeitem.h"
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

BindRole::BindRole(QObject *parent) :
    QObject(parent),
    m_id("id"),
    m_parentId("parent"),
    m_nodeName("node")
{

}

QString BindRole::roleId() const
{
    return m_id;
}

void BindRole::setItemId(const QString &id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit roleIdChanged();
}

QString BindRole::roleParentId() const
{
    return m_parentId;
}

void BindRole::setParentId(const QString &parentId)
{
    if (m_parentId == parentId)
        return;

    m_parentId = parentId;
    emit roleParentIdChanged();
}

QString BindRole::roleNodeName() const
{
    return m_nodeName;
}

void BindRole::setNodeName(const QString &nodeName)
{
    if (m_nodeName == nodeName)
        return;

    m_nodeName = nodeName;
    emit roleNodeNameChanged();
}


Parser::Parser(QObject *parent) :
    QObject(parent),
    m_bindRole(new BindRole(this)),
    m_actions(Actions::None)
{
}

BindRole *Parser::bindRole() const
{
    return m_bindRole;
}

int Parser::actions() const
{
    return m_actions;
}

void Parser::setActions(int actions)
{
    if (m_actions == actions)
        return;

    m_actions = actions;
    emit actionsChanged();
}

QVector<TreeItem *> Parser::array(const QByteArray &source) const
{
    Q_UNUSED(source)
    return QVector<TreeItem *>();
}

TreeItem *Parser::object(const QByteArray &source) const
{
    Q_UNUSED(source)
    return nullptr;
}

bool Parser::isArray(const QByteArray &source) const
{
    Q_UNUSED(source)
    return false;
}

bool Parser::isObject(const QByteArray &source) const
{
    Q_UNUSED(source)
    return false;
}


JsonParser::JsonParser(QObject *parent) :
    Parser(parent)
{
}

TreeItem *JsonParser::load(const QByteArray &source) const
{
    QJsonParseError e;
    TreeItem *rootItem = load(QJsonDocument::fromJson(source, &e).object());
    if (e.error == QJsonParseError::NoError)
    {
        qDebug() << Q_FUNC_INFO << "loaded";
        emit loaded();
    }
    else
    {
        qWarning() << Q_FUNC_INFO << e.errorString() << e.offset;
        emit error(e.errorString(), e.offset);
    }
    return rootItem;
}

QByteArray JsonParser::serialize(TreeItem *parent) const
{
    QJsonObject o = jsonify(parent).toObject();
    return QJsonDocument(o).toJson(QJsonDocument::Compact);
}

QVector<TreeItem *> JsonParser::array(const QByteArray &source) const
{
    QVector<TreeItem *> items;
    QJsonParseError e;
    QJsonDocument doc = QJsonDocument::fromJson(source, &e);
    if (e.error == QJsonParseError::NoError)
    {
        if (doc.isArray())
        {
            for (QJsonValue v : doc.array())
            {
                if (v.isObject())
                {
                    TreeItem *i = load(v.toObject());
                    i->setType(Array);
                    i->setNode(bindRole()->roleNodeName());
                    items << i;
                }
            }
        }
    }
    else
    {
        qWarning() << Q_FUNC_INFO << e.errorString() << e.offset;
        emit error(e.errorString(), e.offset);
    }
    return items;
}

TreeItem *JsonParser::object(const QByteArray &source) const
{
    TreeItem *item = nullptr;
    QJsonParseError e;
    QJsonDocument doc = QJsonDocument::fromJson(source, &e);
    if (e.error == QJsonParseError::NoError)
    {
        if (doc.isObject())
        {
            QJsonObject o = doc.object();
            item = load(o);
            item->setNode(o.value(bindRole()->roleNodeName()).toString());
            item->setType(Object);
        }
    }
    else
    {
        qWarning() << Q_FUNC_INFO << e.errorString() << e.offset;
        emit error(e.errorString(), e.offset);
    }
    return item;
}

bool JsonParser::isArray(const QByteArray &source) const
{
    QJsonParseError e;
    QJsonDocument doc = QJsonDocument::fromJson(source, &e);
    if (e.error == QJsonParseError::NoError)
    {
        return doc.isArray();
    }
    else
    {
        qWarning() << Q_FUNC_INFO << e.errorString() << e.offset;
        return false;
    }
}

bool JsonParser::isObject(const QByteArray &source) const
{
    QJsonParseError e;
    QJsonDocument doc = QJsonDocument::fromJson(source, &e);
    if (e.error == QJsonParseError::NoError)
    {
        return doc.isObject();
    }
    else
    {
        qWarning() << Q_FUNC_INFO << e.errorString() << e.offset;
        return false;
    }
}

TreeItem *JsonParser::load(const QJsonValue &value, TreeItem *parent) const
{
    TreeItem *child = nullptr;
    TreeItem *current = new TreeItem(parent);
    current->setNode("root");

    if (value.isObject())
    {
        for (QString key : value.toObject().keys())
        {
            QJsonValue v = value.toObject().value(key);
            if (!v.isObject() && !hasObject(v))
            {
                current->insert(key, v.toVariant());
            }
            else if (v.isArray() && hasObject(v))
            {
                for (QJsonValue a : v.toArray())
                {
                    child = load(a, current);
                    child->setNode(key);
                    child->setType(Array);
                    current->appendChild(child);
                }
            }
            else if (v.isObject())
            {
                child = load(v, current);
                child->setNode(key);
                child->setType(Object);
                current->appendChild(child);
            }
        }
    }

    return current;
}

bool JsonParser::hasObject(const QJsonValue &value) const
{
    if (!value.isArray())
        return false;

    for (QJsonValue v : value.toArray())
    {
        if (v.isObject())
            return true;
    }

    return false;
}

QJsonValue JsonParser::jsonify(TreeItem *parent) const
{
    if (!parent) return QJsonValue();
    QJsonObject o;
    QJsonArray a;

    if (actions() & Parser::CreateId)
        o.insert(bindRole()->roleId(), QJsonValue::fromVariant(parent->iid()));
    if (actions() & Parser::CreateNode)
        o.insert(bindRole()->roleNodeName(), QJsonValue::fromVariant(parent->node()));
    if (actions() & Parser::BindParent)
    {
        bool hasParent = parent->parentItem() ? true : false;
        o.insert(bindRole()->roleParentId(),
                 QJsonValue::fromVariant(hasParent ? parent->parentItem()->value(bindRole()->roleId()) : 0));
    }
    for (const QString &p : parent->keys())
        o.insert(p, parent->value(p).toJsonValue());

    for (TreeItem *child : parent->children())
    {
        QString key = child->node();
        if (child->type() == Array)
            a.append(jsonify(child));
        else if (child->type() == Object)
            o.insert(key, jsonify(child));

        if (!a.empty())
            o.insert(key, a);
    }

    return o;
}
