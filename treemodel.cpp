#include "treemodel.h"
#include "treeitem.h"
#include "filter.h"
#include "sorter.h"
#include "proxyrole.h"
#include "section.h"
#include "qqml.h"
#include "parser.h"

#include <QDebug>

Collection::Collection(QObject *parent) :
    QAbstractListModel(parent),
    m_rootItem_ptr(new TreeItem(nullptr, this)),
    m_rootItem(QVariant()),
    m_tree(nullptr),
    m_section(nullptr)
{
    connect(this, &Collection::inserted, this, &Collection::invalidateProxyRoles);
    connect(this, &Collection::inserted, this, &Collection::invalidateSorters);
}

Collection::~Collection()
{
}

int Collection::count() const
{
    return rowCount();
}

int Collection::depth() const
{
    return m_rootItem_ptr ? m_rootItem_ptr->level() + 1 : 0;
}

Tree *Collection::tree() const
{
    return m_tree;
}

void Collection::setTree(Tree *tree)
{
    if (m_tree == tree)
        return;

    if (m_tree)
    {
        disconnect(m_tree, &Tree::sourceChanged, this, &Collection::update);
        disconnect(m_tree, &Tree::updated, this, &Collection::update);
        disconnect(m_tree, &Tree::inserted, this, &Collection::onItemInserted);
        disconnect(m_tree, &Tree::removed, this, &Collection::onItemDeleted);
    }

    m_tree = tree;
    connect(m_tree, &Tree::sourceChanged, this, &Collection::update);
    connect(m_tree, &Tree::updated, this, &Collection::update);
    connect(m_tree, &Tree::inserted, this, &Collection::onItemInserted);
    connect(m_tree, &Tree::removed, this, &Collection::onItemDeleted);
    emit treeChanged();
}

Section *Collection::section() const
{
    return m_section;
}

void Collection::setSection(Section *section)
{
    if (m_section == section)
        return;

    if (m_section)
        disconnect(m_section, &Section::invalidated, this, &Collection::update);

    m_section = section;
    connect(m_section, &Section::invalidated, this, &Collection::update);
    emit sectionChanged();
}

QVariant Collection::rootItem() const
{
    return m_rootItem;
}

void Collection::setRootItem(const QVariant &rootItem)
{
    m_rootItem = rootItem;
    emit rootItemChanged();

    if (!m_tree)
    {
        qWarning() << "tree is not set";
        return;
    }

    if (!rootItem.isValid())
        return;

    if (rootItem.canConvert<uint>())
    {
        uint iid = rootItem.toUInt();
        if (iid == 0)
            setRootItem(m_tree->rootItem());
        else
            setRootItem(m_tree->find(iid));
    }
    else if (rootItem.canConvert<TreeItem *>())
    {
        TreeItem *item = rootItem.value<TreeItem *>();
        setRootItem(item);
    }
}

void Collection::setRootItem(TreeItem *rootItem)
{
    if (!rootItem)
        return;

    if (m_rootItem_ptr)
    {
        erase();
        if (m_rootItem_ptr->isQueryItem())
            delete m_rootItem_ptr;
        m_rootItem_ptr = nullptr;
    }

    m_rootItem_ptr = rootItem;
    insert(m_rootItem_ptr->childCount());
}

void Collection::setRowCount(int count)
{
    m_mapping.rowCount = count;
}

int Collection::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_rootItem_ptr ? m_mapping.rowCount : 0;
}

QModelIndex Collection::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if (m_rootItem_ptr && m_rootItem_ptr->childCount() > 0)
        return createIndex(row, column, m_rootItem_ptr->children()[row]);

    return QModelIndex();
}

QVariant Collection::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > rowCount())
        return QVariant();

    const QModelIndex &sourceIndex = mapFromSource(index);
    TreeItem *item = static_cast<TreeItem *>(sourceIndex.internalPointer());
    if (role == ItemRole)
        return QVariant::fromValue(item);
    else if (role == SectionRole && m_section && m_section->enabled())
        return m_section->data(sourceIndex);

    return QVariant();
}

QModelIndex Collection::createIndexFromItem(int index, TreeItem *item)
{
    static Collection model;
    return model.childIndex(index, item);
}

QModelIndex Collection::childIndex(int index, TreeItem *item) const
{
    return createIndex(index, 0, item);
}

QQmlListProperty<Filter> Collection::filters()
{
    return QQmlListProperty<Filter>(this,
                                    nullptr,
                                    &Collection::append_filter,
                                    nullptr, nullptr, nullptr);
}

QQmlListProperty<Sorter> Collection::sorters()
{
    return QQmlListProperty<Sorter>(this,
                                    nullptr,
                                    &Collection::append_sorter,
                                    nullptr, nullptr, nullptr);
}

QQmlListProperty<ProxyRole> Collection::proxyRoles()
{
    return QQmlListProperty<ProxyRole>(this, nullptr,
                                       &Collection::append_proxyRole,
                                       nullptr, nullptr, nullptr);
}

uint Collection::createItem(const QString &node, const QVariantMap &properties)
{
    if (node.isEmpty())
    {
        qWarning() << "node are empty";
        return 0;
    }

    if (m_rootItem_ptr && m_tree && !m_rootItem_ptr->isQueryItem())
    {
        TreeItem *child = new TreeItem;
        for (const QString &key : properties.keys())
            child->insert(key, properties.value(key));

        child->setType(Parser::Array);
        child->setNode(node);
        if (m_tree->insert(m_rootItem_ptr->iid(), child))
        {
            emit itemCreated(child->iid());
            return child->iid();
        }
    }
    return 0;

}

void Collection::deleteItem(uint iid)
{
    if (m_tree && m_tree->remove(iid))
        emit itemDeleted(iid);
}

void Collection::deleteItem(const QVariant &item)
{
    if (!item.isValid())
        return;

    if (item.canConvert<uint>())
    {
        uint iid = item.toUInt();
        deleteItem(iid);
    }
    else if (item.canConvert<TreeItem *>())
    {
        TreeItem *treeItem = item.value<TreeItem *>();
        deleteItem(treeItem->iid());
    }
}

void Collection::mapToSource(/*int row, */const QModelIndex &index)
{
    m_mapping.map(/*row, */index);
}

QModelIndex Collection::mapFromSource(const QModelIndex &index) const
{
    return m_mapping.convert(index);
}

bool Collection::filterAcceptsRow(const QModelIndex &index) const
{
    return std::all_of(m_filters.begin(), m_filters.end(), [&](Filter *filter) {
        return filter->filterRow(index);
    }) || m_filters.isEmpty();
}

void Collection::invalidateSorters()
{
    qDebug() << Q_FUNC_INFO << sender();
    std::sort(m_mapping.indices.begin(), m_mapping.indices.end(),
              [&](const QModelIndex &left, const QModelIndex &right) {
        for (Sorter *sorter : m_sorters)
        {
            if (sorter->enabled())
            {
                int comparison = sorter->compareRows(left, right);
                if (comparison != 0)
                    return comparison < 0;
            }
        }
        return left.row() < right.row();
    });
    if (rowCount() > 0)
        dataChanged(index(0, 0), index(rowCount() - 1, 0));
}

void Collection::invalidateFilters()
{
    if (m_filters.isEmpty())
        return;

    qDebug() << Q_FUNC_INFO << sender();

    for (int i = 0; i < m_rootItem_ptr->childCount(); ++i)
    {
        if (filterAcceptsRow(index(i, 0)) && !m_mapping.contains(index(i, 0)))
        {
            beginInsertRows(QModelIndex(), rowCount()/* - 1*/, rowCount()/* - 1*/);
            setRowCount(rowCount() + 1);
            mapToSource(index(i, 0));
            endInsertRows();
            ++m_filteredRows;
            qDebug() << "[insert]" << index(i, 0);
        }
        else if (!filterAcceptsRow(index(i, 0)) && m_mapping.contains(index(i, 0))
                 && m_mapping.indices.indexOf(index(i, 0)) != -1)
        {
            int removeIndex = m_mapping.indices.indexOf(index(i, 0));
            removeIndex = removeIndex >= rowCount() ? rowCount() - 1 : removeIndex;
            beginRemoveRows(QModelIndex(), removeIndex, removeIndex);
            m_mapping.indices.takeAt(removeIndex);
            setRowCount(rowCount() - 1);
            endRemoveRows();
            --m_filteredRows;
            qDebug() << "[remove]" << index(i, 0);
        }
    }

}

void Collection::createProxyRole(const QModelIndex &index)
{
    for (ProxyRole *proxyRole : m_proxyRoles)
        proxyRole->create(index);
}

void Collection::invalidateProxyRoles()
{
    if (m_proxyRoles.isEmpty())
        return;

    if (!m_rootItem_ptr)
        return;

    qDebug() << Q_FUNC_INFO << sender();
    for (int i = 0; i < m_rootItem_ptr->childCount(); ++i)
        createProxyRole(index(i, 0));
}


void Collection::insert(int size)
{
    qDebug() << Q_FUNC_INFO << "size:" << size;
    int row = 0;
    for (int i = 0; i < size; ++i)
    {
        TreeItem *item = static_cast<TreeItem *>(index(i, 0).internalPointer());
        connect(item, &TreeItem::valueChanged, this, &Collection::invalidateProxyRoles);
        connect(item, &TreeItem::valueChanged, this, &Collection::invalidateFilters);
        connect(item, &TreeItem::valueChanged, this, &Collection::invalidateSorters);

        createProxyRole(index(i, 0));

        if (filterAcceptsRow(index(i, 0)))
        {
            mapToSource(/*row, */index(i, 0));
            row++;
        }
    }
    setRowCount(row);

    if (rowCount() > 0)
    {
        beginInsertRows(QModelIndex(), 0, rowCount() - 1);
        endInsertRows();
    }
    emit inserted();
    emit countChanged();
}

void Collection::erase()
{
    qDebug() << Q_FUNC_INFO;
    if (rowCount() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        endRemoveRows();
    }
    setRowCount(0);
    m_mapping.clear();
    emit countChanged();
}

void Collection::reset()
{
    qDebug() << Q_FUNC_INFO;
    beginResetModel();
    m_rootItem_ptr = nullptr;
    setRowCount(0);
    m_mapping.clear();
    endResetModel();
    emit countChanged();
}

void Collection::update()
{
    if (!m_rootItem_ptr->isQueryItem())
        setRootItem(rootItem());
}

void Collection::onItemInserted(uint parentIid, uint )
{
    if (m_rootItem_ptr && !m_rootItem_ptr->isQueryItem() && m_rootItem_ptr->iid() == parentIid)
    {
        update();
    }
}

void Collection::onItemDeleted(uint parentIid, uint iid)
{
    if (m_rootItem_ptr && !m_rootItem_ptr->isQueryItem())
    {
        if (m_rootItem_ptr->iid() == iid || m_rootItem_ptr->isChildOf(iid))
        {
            reset();
            setRootItem(QVariant());
        }
        else if (m_rootItem_ptr->iid() == parentIid)
        {
            update();
        }
    }

}

void Collection::append_filter(QQmlListProperty<Filter> *list, Filter *filter)
{
    Collection *model = qobject_cast<Collection *>(list->object);
    if(model)
    {
        filter->setParent(model);
        QObject::connect(filter, &Filter::invalidated, model, &Collection::invalidateFilters);
        QObject::connect(filter, &Filter::invalidated, model, &Collection::invalidateSorters);
        model->m_filters.append(filter);
        filter->invalidated();
    }
}

void Collection::append_sorter(QQmlListProperty<Sorter> *list, Sorter *sorter)
{
    Collection *model = qobject_cast<Collection *>(list->object);
    if(model)
    {
        sorter->setParent(model);
        QObject::connect(sorter, &Sorter::invalidated, model, &Collection::invalidateSorters);
        model->m_sorters.append(sorter);
        sorter->invalidated();
    }
}

void Collection::append_proxyRole(QQmlListProperty<ProxyRole> *list, ProxyRole *proxyRole)
{
    Collection *model = qobject_cast<Collection *>(list->object);
    if(model)
    {
        proxyRole->setParent(model);
        model->m_proxyRoles.append(proxyRole);
    }
}

QHash<int, QByteArray> Collection::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ItemRole] = "item";
    roles[SectionRole] = "section";
    return roles;
}

#define URI "com.qt.tree"
static void registerType()
{
    qmlRegisterType<Collection>(URI, 1, 0, "Collection");
    qmlRegisterUncreatableType<Section>(URI, 1, 0, "Section", "Section is an abstract class");
    qmlRegisterType<RoleSection>(URI, 1, 0, "RoleSection");
    qmlRegisterType<ParentSection>(URI, 1, 0, "ParentSection");
    qmlRegisterType<SwitchSection>(URI, 1, 0, "SwitchSection");
    qmlRegisterInterface<Filter>("Filter");
    qmlRegisterType<ValueFilter>(URI, 1, 0, "ValueFilter");
    qmlRegisterType<IndexFilter>(URI, 1, 0, "IndexFilter");
    qmlRegisterType<RangeFilter>(URI, 1, 0, "RangeFilter");
    qmlRegisterType<RegExpFilter>(URI, 1, 0, "RegExpFilter");
    qmlRegisterType<ExpressionFilter>(URI, 1, 0, "ExpressionFilter");
    qmlRegisterType<ChildrenFilter>(URI, 1, 0, "ChildrenFilter");
    qmlRegisterType<AnyOf>(URI, 1, 0, "AnyOf");
    qmlRegisterType<AllOf>(URI, 1, 0, "AllOf");
    qmlRegisterType<Contains>(URI, 1, 0, "Contains");
    qmlRegisterInterface<Sorter>("Sorter");
    qmlRegisterType<RoleSorter>(URI, 1, 0, "RoleSorter");
    qmlRegisterType<FilterSorter>(URI, 1, 0, "FilterSorter");
    qmlRegisterType<ParentSorter>(URI, 1, 0, "ParentSorter");
    qmlRegisterType<StringSorter>(URI, 1, 0, "StringSorter");
    qmlRegisterType<ExpressionSorter>(URI, 1, 0, "ExpressionSorter");
    qmlRegisterInterface<ProxyRole>("ProxyRole");
    qmlRegisterType<JoinRole>(URI, 1, 0, "JoinRole");
    qmlRegisterType<FilterRole>(URI, 1, 0, "FilterRole");
    qmlRegisterType<ExpressionRole>(URI, 1, 0, "ExpressionRole");
    qmlRegisterType<QueryRole>(URI, 1, 0, "QueryRole");
}
Q_COREAPP_STARTUP_FUNCTION(registerType)
