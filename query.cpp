#include "query.h"
#include "treemodel.h"
#include "treeitem.h"

#include <QQmlEngine>
#include <QQmlInfo>

Query::Query(QQmlEngine *engine, QObject *parent) :
    QObject(parent),
    m_engine(engine),
    m_model(nullptr)
{
    m_engine->setObjectOwnership(this, QQmlEngine::CppOwnership);
    m_self = m_engine->newQObject(this);
}

Query::~Query()
{
}

QJSValue Query::from(Collection *model)
{
    if (!model || !model->inherits("Collection"))
        qWarning() << "model must be inherited from Collection";

    m_queryData.clear();
    m_model = model;
    return self();
}

QJSValue Query::select(const QString &role)
{
    m_role = role;
    return self();
}

QJSValue Query::all(const QVariant &where)
{
    if (!m_model || !m_model->tree())
        qmlInfo(this) << "model is not set";

    QVector<TreeItem *> all;
    allOf(m_role, where, all, m_model && m_model->tree() ? m_model->tree()->rootItem() : nullptr);
    m_queryData = all;
    return self();
}

QJSValue Query::any(const QVariant &where)
{
    if (!m_model || !m_model->tree())
        qmlInfo(this) << "model is not set";

    QVector<TreeItem *> any;
    if (m_model && m_model->tree())
        any << m_model->tree()->find(m_role, where);

    m_queryData = any;
    return self();
}

QJSValue Query::filter(QJSValue filter)
{
    QVector<TreeItem *> filtered;
    if (filter.isCallable())
    {
        for (TreeItem *i : m_queryData)
        {
            QJSValue value = m_engine->newQObject(i);
            if (filterRow(filter, QJSValueList() << value))
                filtered << i;
        }
    }
    m_queryData = filtered;
    return self();
}

int Query::count()
{
    return m_queryData.size();
}

void Query::commit()
{
    if (!m_model || !m_model->tree())
    {
        qmlInfo(this) << "model is not set";
        return;
    }

    QueryItem *queryItem = new QueryItem(m_model);
    queryItem->setData(m_queryData);
    m_model->setRootItem(queryItem);
}

void Query::revert()
{
    if (!m_model || !m_model->tree())
    {
        qmlInfo(this) << "model is not set";
        return;
    }

    m_model->setRootItem(m_model->rootItem());
}

void Query::allOf(const QString &key, const QVariant &value, QVector<TreeItem *> &list, TreeItem *parent) const
{
    if (!parent) return;

    for (TreeItem *i : parent->children())
    {
        if (i->value(key) == value)
            list.append(i);

        allOf(key, value, list, i);
    }
}

bool Query::filterRow(QJSValue fn, const QJSValueList &args) const
{
    QJSValue result = fn.call(args);
    if (result.isError())
    {
        QString fileName = result.property("fileName").toString();
        QString lineNumber = result.property("lineNumber").toString();
        qWarning("%s: %s (%s)",
                 qUtf8Printable(result.toString()),
                 qUtf8Printable(fileName),
                 qUtf8Printable(lineNumber));
        return true;
    }

    return result.toBool();
}

#define URI "com.qt.tree"
#include <QQmlEngine>
#include <QGuiApplication>
static QObject *query_provider(QQmlEngine *engine, QJSEngine *)
{
    return new Query(engine);
}

static void registerTypes()
{
    qmlRegisterSingletonType<Query>(URI, 1, 0, "Query", query_provider);
}
Q_COREAPP_STARTUP_FUNCTION(registerTypes)

