#ifndef CONTAINER_H
#define CONTAINER_H

#include <QQmlListProperty>

class Filter;
class Container
{
public:
    virtual ~Container();
    QQmlListProperty<Filter> filters();

protected:
    QList<Filter *> m_filters;

private:
    virtual void onFilterAppended(Filter *filter) = 0;
    static void append_filter(QQmlListProperty<Filter> *list, Filter *filter);
};
#define Container_iid "com.qt.tree.Container"
Q_DECLARE_INTERFACE(Container, Container_iid)
#endif // CONTAINER_H
