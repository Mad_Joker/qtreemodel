#include "treeeditor.h"
#include "treeitem.h"
#include "parser.h"

ItemData::ItemData(QObject *parent) : QObject(parent)
{

}

QString ItemData::node() const
{
    return m_node;
}

void ItemData::setNode(QString node)
{
    if (m_node == node)
        return;

    m_node = node;
    emit nodeChanged();
}

int ItemData::type() const
{
    return m_type;
}

void ItemData::setType(int type)
{
    if (m_type == type)
        return;

    m_type = type;
    emit typeChanged();
}

TreeEditor::TreeEditor(QObject *parent) :
    QObject(parent),
    m_itemData(new ItemData(this))
{

}

Tree *TreeEditor::tree() const
{
    return m_tree;
}

void TreeEditor::setTree(Tree *tree)
{
    if (m_tree == tree)
        return;

    m_tree = tree;
    emit treeChanged();
}

Parser *TreeEditor::parser() const
{
    return m_parser;
}

void TreeEditor::setParser(Parser *parser)
{
    if (m_parser == parser)
        return;

    m_parser = parser;
    emit parserChanged();
}

ItemData *TreeEditor::itemData() const
{
    return m_itemData;
}

uint TreeEditor::rootItemIid() const
{
    return m_rootItemIid;
}

void TreeEditor::setRootItemIid(uint rootItemIid)
{
    if (m_rootItemIid == rootItemIid)
        return;

    m_rootItemIid = rootItemIid;
    setRootItem(rootItemIid);
    emit rootItemIidChanged();
}

uint TreeEditor::create(const QVariantMap &properties)
{
    if (m_rootItem && isTreeExist())
    {
        TreeItem *child = new TreeItem;
        for (const QString &key : properties.keys())
            child->insert(key, properties.value(key));

        child->setType(m_itemData->type());
        child->setNode(m_itemData->node());
        if (m_tree->insert(m_rootItem->iid(), child))
        {
            emit updated();
            return child->iid();
        }
    }
    return 0;
}

void TreeEditor::remove(uint iid)
{
    if (m_tree->remove(iid))
        emit updated();

    m_tree->rootItem()->dump();
    m_tree->rootItem()->dumpObjectInfo();
    m_tree->rootItem()->dumpObjectTree();
}

QByteArray TreeEditor::serialize() const
{
    if (!m_parser)
    {
        qWarning() << "parser is not set";
        return QByteArray();
    }

    qDebug() << m_parser->serialize(m_rootItem);
    return m_parser->serialize(m_rootItem);
}

bool TreeEditor::isTreeExist() const
{
    if (!m_tree)
    {
        qWarning() << "tree is not set";
        return false;
    }
    return true;
}

void TreeEditor::setRootItem(uint iid)
{
    if (isTreeExist())
    {
        TreeItem *item = m_tree->find(iid);
        if (item)
            m_rootItem = item;
    }
}

