#ifndef SECTION_H
#define SECTION_H

#include <QObject>
#include "container.h"
#include <QtQml>

class Section : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

public:
    explicit Section(QObject *parent = nullptr);
    virtual ~Section() {}

    bool enabled() const;
    void setEnabled(bool enabled);

    virtual QVariant data(const QModelIndex &index) const = 0;

signals:
    void enabledChanged();
    void invalidated();

protected:
    void invalidate();

private:
    bool m_enabled;
};


class RoleSection : public Section
{
    Q_OBJECT
    Q_PROPERTY(QString role READ role WRITE setRole NOTIFY roleChanged)

public:
    explicit RoleSection(QObject *parent = nullptr);
    ~RoleSection() override {}

    QString role() const;
    void setRole(const QString &role);

    QVariant data(const QModelIndex &index) const override;

signals:
    void roleChanged();

private:
    QString m_role;
};


class ParentSection : public RoleSection
{
    Q_OBJECT
public:
    explicit ParentSection(QObject *parent = nullptr);
    ~ParentSection() override {}

    QVariant data(const QModelIndex &index) const override;
};


class SwitchSectionAttached : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)
public:
    SwitchSectionAttached(QObject* parent);

    QVariant value() const;
    void setValue(const QVariant &value);

Q_SIGNALS:
    void valueChanged();

private:
    QVariant m_value;
};

class SwitchSection : public Section, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(Section* defaultSection READ defaultSection WRITE setDefaultSection NOTIFY defaultSectionChanged)
    Q_PROPERTY(QVariant defaultValue READ defaultValue WRITE setDefaultValue NOTIFY defaultValueChanged)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit SwitchSection(QObject *parent = nullptr);
    ~SwitchSection() override {}
    static SwitchSectionAttached *qmlAttachedProperties(QObject* object);

    Section *defaultSection() const;
    void setDefaultSection(Section *defaultSection);

    QVariant defaultValue() const;
    void setDefaultValue(const QVariant &defaultValue);

    QVariant data(const QModelIndex &index) const override;

signals:
    void defaultSectionChanged();
    void defaultValueChanged();
    void filtersChanged();

private:
    void onFilterAppended(Filter *filter) override;

private:
    Section *m_defaultSection;
    QVariant m_defaultValue;
};
QML_DECLARE_TYPEINFO(SwitchSection, QML_HAS_ATTACHED_PROPERTIES)
#endif // SECTION_H
