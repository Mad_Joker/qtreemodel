#include "treeitem.h"
#include "qqml.h"
#include <QQmlEngine>
#include <QQmlContext>
#include <QUuid>

#include <QGuiApplication>
#include <QDebug>
#include "parser.h"

TreeItem::TreeItem(QObject *parent) :
    QQmlPropertyMap(this, parent),
    m_parentItem(nullptr),
    m_children(QVector<TreeItem *>())
{
    setIid(uniqueIid());
    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
}

TreeItem::TreeItem(TreeItem *parentItem, QObject *parent) :
    QQmlPropertyMap(this, parent),
    m_parentItem(parentItem),
    m_children(QVector<TreeItem *>())
{
    setIid(uniqueIid());
    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
    if (parentItem)
        setLevel(parentItem->level() + 1);
}

TreeItem::~TreeItem()
{
}

uint TreeItem::iid() const
{
    return m_iid;
}

void TreeItem::setIid(uint iid)
{
    if (m_iid == iid)
        return;

    m_iid = iid;
    emit iidChanged();
}

int TreeItem::level() const
{
    return m_level;
}

void TreeItem::setLevel(const int level)
{
    if (m_level == level)
        return;

    m_level = level;
    emit levelChanged();
}

void TreeItem::adjustLevels()
{
    for (TreeItem *item : m_children)
    {
        item->setLevel(level() + 1);
        item->adjustLevels();
    }
}

QString TreeItem::node() const
{
    return objectName();
}

void TreeItem::setNode(const QString &node)
{
    if (objectName() == node)
        return;

    setObjectName(node);
    emit nodeChanged();
}

int TreeItem::type() const
{
    return m_type;
}

void TreeItem::setType(int type)
{
    if (m_type == type)
        return;

    m_type = type;
    emit typeChanged();
}

TreeItem *TreeItem::parentItem() const
{
    return m_parentItem;
}

void TreeItem::setParentItem(TreeItem *parent)
{
    m_parentItem = parent;
}

void TreeItem::adjustParents()
{
    for (TreeItem *item : m_children)
    {
        item->setParentItem(this);
        item->adjustParents();
    }
}

QVariant TreeItem::value(const QString &key) const
{
    return this->property(key.toUtf8().constData());
}

bool TreeItem::hasProperty(const QString &property) const
{
    return this->property(property.toUtf8().constData()).isValid();
}

QVector<TreeItem *> TreeItem::children() const
{
    return m_children;
}

TreeItem *TreeItem::child(int index) const
{
    if (index < 0 || index >= m_children.size())
        return nullptr;

    return m_children[index];
}

int TreeItem::childIndex(TreeItem *child)
{
    return m_children.indexOf(child);
}

int TreeItem::index() const
{
    return m_parentItem ? m_parentItem->m_children.indexOf(const_cast<TreeItem *>(this)) : 0;
}

bool TreeItem::isChildOf(uint iid) const
{
    if (!parentItem())
        return false;

    if (parentItem()->iid() == iid)
        return true;

    return parentItem()->isChildOf(iid);
}

void TreeItem::appendChild(TreeItem *child)
{
    child->setParent(this);
    child->setParentItem(this);
    if (child->level() == 0)
        child->setLevel(level() + 1);

    if (child->hasChildren())
    {
        child->adjustLevels();
        child->adjustParents();
    }
    m_children << child;
}

void TreeItem::removeChild(int index)
{
    TreeItem *child = m_children.takeAt(index);
    child->disconnect();
    child->deleteLater();
    child = nullptr;
}

bool TreeItem::hasChild(TreeItem *child) const
{
    return m_children.contains(child);
}

bool TreeItem::hasChildren() const
{
    return !m_children.isEmpty();
}

int TreeItem::childCount() const
{
    return m_children.size();
}

uint TreeItem::uniqueIid()
{
    return qHash(QUuid::createUuid());
}


void TreeItem::dump(TreeItem *item, QString tab)
{
     if (!item)
         item = this;

     qDebug() << qPrintable(tab)
              << "iid:" << item->iid() << "node:" << item->node() << "depth:" << item->level()
              << "parentItem:" << item->parentItem()
              << "\n" << qPrintable(tab)
              << item->keys();

     for (TreeItem *i : item->children())
         dump(i, QString().fill(QChar('\t'), item->level() + 1));
}


Tree::Tree(QObject *parent) :
    QObject(parent),
    m_rootItem(new TreeItem(nullptr, this)),
    m_source(QString()),
    m_parser(nullptr)
{
    QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
}

Tree::~Tree()
{
}

Tree &Tree::instance(QObject *parent)
{
    static Tree tree(parent);
    return tree;
}

TreeItem *Tree::rootItem() const
{
    return m_rootItem;
}

QString Tree::source() const
{
    return m_source;
}

void Tree::setSource(QString source)
{
    if (m_source == source)
        return;

    m_source = source;
    load(source.toUtf8());
    qDebug() << serialize(rootItem());
    dump();
    emit sourceChanged();
}

Parser *Tree::parser() const
{
    return m_parser;
}

void Tree::setParser(Parser *parser)
{
    if (m_parser == parser)
        return;

    m_parser = parser;
    connect(parser, &Parser::loaded, this, &Tree::loaded);
    connect(parser, &Parser::error, this, &Tree::error);
    emit parserChanged();
}

void Tree::load(const QByteArray &serializedString)
{
    if (parser())
        m_rootItem = parser()->load(serializedString);
}

QByteArray Tree::serialize(TreeItem *parent) const
{
    if (parser())
        return parser()->serialize(parent);

    return QByteArray();
}

TreeItem *Tree::find(const QString &name, TreeItem *parent)
{
    return find_helper(name, parent ? parent : m_rootItem);
}

TreeItem *Tree::find(uint iid, TreeItem *parent)
{
    return find_helper(iid, parent ? parent : m_rootItem);
}

TreeItem *Tree::find(const QString &property, const QVariant &value, TreeItem *parent) const
{
    return find_helper(property, value, parent ? parent : m_rootItem);
}

bool Tree::insert(uint parentIid, TreeItem *child)
{
    TreeItem *parent = find(parentIid, m_rootItem);
    if (parent && child)
    {
        parent->appendChild(child);
        emit inserted(parentIid, child->iid());
        return true;
    }
    return false;
}

void Tree::insert(const QVariant &parentId, TreeItem *child)
{
    if (m_parser)
    {
        TreeItem *parent = find(m_parser->bindRole()->roleId(), parentId);
        parent = parent ? parent : m_rootItem;
        if (parent && child)
        {
            parent->appendChild(child);
            emit updated();
        }
    }
}

/* json must be as below:
 * parentId = bind.parentId
 * [
 *  {
 *      parentId: "parentId"
 *      id: "id"
 *      property: value
 *      children: []
 *  },
 *  {
 *      ...
 *  }
 * ]
*/
void Tree::insert(const QByteArray &serialized)
{
    if (parser())
    {
        if (parser()->isArray(serialized))
        {
            QVector<TreeItem *> items = parser()->array(serialized);
            for (TreeItem *item : items)
                if (item->hasProperty(m_parser->bindRole()->roleParentId()))
                    insert(item->value(m_parser->bindRole()->roleParentId()), item);
        }
        else if (parser()->isObject(serialized))
            if (TreeItem *item = parser()->object(serialized))
                if (item->hasProperty(m_parser->bindRole()->roleParentId()))
                    insert(item->value(m_parser->bindRole()->roleParentId()), item);
    }
}

void Tree::remove(const QVariant &id)
{
    (void)id;
    //TODO: implement remove;
}

bool Tree::remove(uint iid)
{
    TreeItem *item = find(iid, m_rootItem);
    TreeItem *parent = item->parentItem();
    if (item && parent)
    {
        int index = parent->childIndex(item);
        parent->removeChild(index);
        emit removed(parent->iid(), iid);
        return true;
    }
    else if (!parent && item == m_rootItem)
    {
        emit removed(0, iid);
        delete m_rootItem;
        return true;
    }
    return false;
}

QVariant Tree::item(const QString key, const QVariant &value) const
{
    return QVariant::fromValue(find(key, value));
}

QVariant Tree::children(const QString key, const QVariant &value) const
{
    return QVariant::fromValue(find(key, value)->children());
}

/*
 * update an one item. Childs will be ignored.
 * for recursive update use merge()
 * json must be as below:
 * bind.id = "id"
 * {
 *  "id": "unique string" - mandatory
 *  "property: "value"
 *  "children": []
 * }"
*/
void Tree::update(const QByteArray &attributes)
{
    TreeItem *i = nullptr;
    if (parser())
    {
        QString id = m_parser->bindRole()->roleId();
        TreeItem *o = parser()->object(attributes);
        qDebug() << o->node();
        if (id.isEmpty())
        {
            qDebug() << Q_FUNC_INFO << "hint.itemId is not provided";
            return;
        }

        if (!o->hasProperty(id))
        {
            qDebug() << Q_FUNC_INFO << "attribute" << id << "not exist";
            return;
        }

        i = find(id, o->value(id));
        if ( i )    //update
        {
            for (const QString &key : o->keys())
            {
                if (key == id || key == m_parser->bindRole()->roleParentId() ||
                                 key == m_parser->bindRole()->roleNodeName())
                    continue;

                i->insert(key, o->value(key));
            }
        }
        else if (m_parser->actions() & Parser::InsertOnUpdate)    //insert new
        {
            insert(attributes);
        }
    }
}

void Tree::dump()
{
    rootItem()->dump();
}

void Tree::test()
{

}

TreeItem *Tree::find_helper(uint iid, TreeItem *parent) const
{
    if (!parent) return nullptr;
    if (parent->iid() == iid) return parent;

    for (TreeItem *i : parent->children())
    {
        if (iid != 0 && i->iid() == iid)
            return i;

        TreeItem *child = find_helper(iid, i);
        if (child) return child;
    }
    return nullptr;
}

TreeItem *Tree::find_helper(const QString &name, TreeItem *parent) const
{
    if (!parent) return parent;

    for (TreeItem *i : parent->children())
    {
        if (i->node() == name)
            return i;

        TreeItem *child = find_helper(name, i);
        if (child) return child;
    }
    return nullptr;
}

TreeItem *Tree::find_helper(const QString &key, const QVariant &value, TreeItem *parent) const
{
    if (!parent) return parent;

    for (TreeItem *i : parent->children())
    {
        if (i->hasProperty(key) && i->value(key) == value)
            return i;

        TreeItem *child = find_helper(key, value, i);
        if (child) return child;
    }
    return nullptr;
}

#define URI "com.qt.tree"
static void registerType()
{
    qmlRegisterType<Tree>(URI, 1, 0, "Tree");
    qmlRegisterType<BindRole>(URI, 1, 0, "Hint");
    qmlRegisterUncreatableType<Parser>(URI, 1, 0, "Parser", "Parser is an abstract class");
    qmlRegisterType<JsonParser>(URI, 1, 0, "JsonParser");
}
Q_COREAPP_STARTUP_FUNCTION(registerType)
