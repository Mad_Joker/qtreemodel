#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QVector>
#include <QJsonParseError>

/*
 * bind: ["Id:id", "Parent:parent", "Node:node"]
*/
class BindRole : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString roleId READ roleId WRITE setItemId NOTIFY roleIdChanged)
    Q_PROPERTY(QString roleParentId READ roleParentId WRITE setParentId NOTIFY roleParentIdChanged)
    Q_PROPERTY(QString roleNodeName READ roleNodeName WRITE setNodeName NOTIFY roleNodeNameChanged)

public:
    enum Key {
        Id,
        Parent,
        Node
    };

public:
    explicit BindRole(QObject *parent = nullptr);

    QString roleId() const;
    void setItemId(const QString &id);

    QString roleParentId() const;
    void setParentId(const QString &roleParentId);

    QString roleNodeName() const;
    void setNodeName(const QString &roleNodeName);

signals:
    void roleIdChanged();
    void roleParentIdChanged();
    void roleNodeNameChanged();

private:
    QString m_id;
    QString m_parentId;
    QString m_nodeName;
};

class TreeItem;
class Parser : public QObject
{
    Q_OBJECT
    Q_PROPERTY(BindRole* bindRole READ bindRole NOTIFY bindRoleChanged)
    Q_PROPERTY(int actions READ actions WRITE setActions NOTIFY actionsChanged)
    Q_ENUMS(NodeType)
    Q_ENUMS(Actions)

public:
    enum NodeType {
        Undefined,
        Array,
        Object
    };

    enum Actions {
        None            = 0,
        InsertOnUpdate  = 1 << 0,
        BindParent      = 1 << 1,
        CreateId        = 1 << 2,
        CreateNode      = 1 << 3,
        CreateAll       = CreateId | CreateNode
    };

public:
    explicit Parser(QObject *parent = nullptr);
    virtual ~Parser() {}

    BindRole *bindRole() const;

    int actions() const;
    void setActions(int actions);

    virtual TreeItem *load(const QByteArray &source) const = 0;
    virtual QByteArray serialize(TreeItem *parent) const = 0;

    virtual QVector<TreeItem *> array(const QByteArray &source) const;
    virtual TreeItem *object(const QByteArray &source) const;
    virtual bool isArray(const QByteArray &source) const;
    virtual bool isObject(const QByteArray &source) const;

signals:
    void loaded() const;
    void error(QString errorString, int offset) const;
    void bindRoleChanged();
    void actionsChanged();

private:
    BindRole *m_bindRole;
    int m_actions;
};

class JsonParser : public Parser
{
    Q_OBJECT
public:
    explicit JsonParser(QObject *parent = nullptr);
    ~JsonParser() override {}

    TreeItem *load(const QByteArray &source) const override;
    QByteArray serialize(TreeItem *parent) const override;

    QVector<TreeItem *> array(const QByteArray &source) const override;
    TreeItem *object(const QByteArray &source) const override;
    bool isArray(const QByteArray &source) const override;
    bool isObject(const QByteArray &source) const override;

private:
    TreeItem *load(const QJsonValue &value, TreeItem *parent = nullptr) const;
    bool hasObject(const QJsonValue &value) const;
    QJsonValue jsonify(TreeItem *parent) const;
};

#endif // PARSER_H
