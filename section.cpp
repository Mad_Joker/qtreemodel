#include "section.h"
#include "treeitem.h"
#include "filter.h"

Section::Section(QObject *parent) :
    QObject(parent),
    m_enabled(false)
{

}

bool Section::enabled() const
{
    return m_enabled;
}

void Section::setEnabled(bool enabled)
{
    if (m_enabled == enabled)
        return;

    m_enabled = enabled;
    emit enabledChanged();
    emit invalidated();
}

void Section::invalidate()
{
    if (m_enabled)
        emit invalidated();
}



RoleSection::RoleSection(QObject *parent) :
    Section(parent),
    m_role(QString())
{

}

QString RoleSection::role() const
{
    return m_role;
}

void RoleSection::setRole(const QString &role)
{
    if (m_role == role)
        return;

    m_role = role;
    emit roleChanged();
    invalidate();
}

QVariant RoleSection::data(const QModelIndex &index) const
{
    return static_cast<TreeItem *>(index.internalPointer())->value(role());
}


ParentSection::ParentSection(QObject *parent) :
    RoleSection(parent)
{

}

QVariant ParentSection::data(const QModelIndex &index) const
{
    TreeItem *parent = static_cast<TreeItem *>(index.internalPointer())->parentItem();
    return parent ? parent->value(role()) : QVariant();
}


SwitchSectionAttached::SwitchSectionAttached(QObject *parent) :
    QObject(parent)
{
    if (!qobject_cast<Filter*>(parent))
        qmlInfo(parent) << "SwitchRole must be attached to a Filter";
}

QVariant SwitchSectionAttached::value() const
{
    return m_value;
}

void SwitchSectionAttached::setValue(const QVariant &value)
{
    if (m_value == value)
        return;

    m_value = value;
    Q_EMIT valueChanged();
}


SwitchSection::SwitchSection(QObject *parent) :
    Section(parent),
    m_defaultSection(nullptr)
{

}

SwitchSectionAttached *SwitchSection::qmlAttachedProperties(QObject *object)
{
    return new SwitchSectionAttached(object);
}

Section *SwitchSection::defaultSection() const
{
    return m_defaultSection;
}

void SwitchSection::setDefaultSection(Section *defaultSection)
{
    if (m_defaultSection == defaultSection)
        return;

    m_defaultSection = defaultSection;
    emit defaultSectionChanged();
    invalidate();
}

QVariant SwitchSection::defaultValue() const
{
    return m_defaultValue;
}

void SwitchSection::setDefaultValue(const QVariant &defaultValue)
{
    if (m_defaultValue == defaultValue)
        return;

    m_defaultValue = defaultValue;
    emit defaultValueChanged();
    invalidate();
}

QVariant SwitchSection::data(const QModelIndex &index) const
{
    for (auto filter: m_filters)
    {
        if (!filter->enabled())
            continue;

        if (filter->filterRow(index))
        {
            auto attached = static_cast<SwitchSectionAttached *>(qmlAttachedPropertiesObject<SwitchSection>(filter, false));
            if (!attached)
            {
                qWarning() << "No SwitchRole.value provided for this filter" << filter;
                continue;
            }
            QVariant value = attached->value();
            if (!value.isValid())
            {
                qWarning() << "No SwitchRole.value provided for this filter" << filter;
                continue;
            }
            return value;
        }
    }
    if (m_defaultSection && m_defaultSection->enabled())
        return m_defaultSection->data(index);
    return m_defaultValue;
}

void SwitchSection::onFilterAppended(Filter *filter)
{
    connect(filter, &Filter::invalidated, this, &SwitchSection::invalidate);
    SwitchSectionAttached *attached = static_cast<SwitchSectionAttached *>(qmlAttachedPropertiesObject<SwitchSection>(filter, true));
    connect(attached, &SwitchSectionAttached::valueChanged, this, &SwitchSection::invalidate);
    invalidate();
}
