#ifndef FILTER_H
#define FILTER_H

#include <QObject>
#include <QVariant>
#include <QQmlScriptString>
#include <QQmlParserStatus>
#include "container.h"

class Filter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)

public:
    explicit Filter(QObject *parent = nullptr);
    virtual ~Filter() {}

    bool enabled() const;
    void setEnabled(bool enabled);

    virtual bool filterRow(const QModelIndex &index) const = 0;

    Q_INVOKABLE void refilter();
signals:
    void enabledChanged();
    void invalidated();

protected:
    void invalidate();

private:
    bool m_enabled;
};

class IndexFilter : public Filter
{
    Q_OBJECT
    Q_PROPERTY(qint32 minimum READ minimum WRITE setMinimum NOTIFY minimumChanged)
    Q_PROPERTY(qint32 maximum READ maximum WRITE setMaximum NOTIFY maximumChanged)

public:
    explicit IndexFilter(QObject *parent = nullptr);
    ~IndexFilter() override {}

    qint32 minimum() const;
    void setMinimum(qint32 minimum);

    qint32 maximum() const;
    void setMaximum(qint32 maximum);

    bool filterRow(const QModelIndex &index) const override;

signals:
    void minimumChanged();
    void maximumChanged();

private:
    qint32 m_minimum;
    qint32 m_maximum;
};

class RoleFilter : public Filter
{
    Q_OBJECT
    Q_PROPERTY(QString role READ role WRITE setRole NOTIFY roleChanged)

public:
    explicit RoleFilter(QObject *parent = nullptr);

    QString role() const;
    void setRole(const QString &role);

signals:
    void roleChanged();

private:
    QString m_role;
};


class ValueFilter : public RoleFilter
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)

public:
    explicit ValueFilter(QObject *parent = nullptr);
    ~ValueFilter() override {}

    QVariant value() const;
    void setValue(const QVariant &value);

    bool filterRow(const QModelIndex &index) const override;

signals:
    void valueChanged();

private:
    QVariant m_value;
};


class RangeFilter : public RoleFilter
{
    Q_OBJECT
    Q_PROPERTY(QVariant minimumValue READ minimumValue WRITE setMinimumValue NOTIFY minimumValueChanged)
    Q_PROPERTY(bool minimumInclusive READ minimumInclusive WRITE setMinimumInclusive NOTIFY minimumInclusiveChanged)
    Q_PROPERTY(QVariant maximumValue READ maximumValue WRITE setMaximumValue NOTIFY maximumValueChanged)
    Q_PROPERTY(bool maximumInclusive READ maximumInclusive WRITE setMaximumInclusive NOTIFY maximumInclusiveChanged)

public:
    explicit RangeFilter(QObject *parent = nullptr);
    ~RangeFilter() override {}

    QVariant minimumValue() const;
    void setMinimumValue(const QVariant &minimumValue);

    bool minimumInclusive() const;
    void setMinimumInclusive(bool minimumInclusive);

    QVariant maximumValue() const;
    void setMaximumValue(const QVariant &maximumValue);

    bool maximumInclusive() const;
    void setMaximumInclusive(bool maximumInclusive);

    bool filterRow(const QModelIndex &index) const override;

signals:
    void minimumValueChanged();
    void minimumInclusiveChanged();
    void maximumValueChanged();
    void maximumInclusiveChanged();

private:
    QVariant m_minimumValue;
    bool m_minimumInclusive;
    QVariant m_maximumValue;
    bool m_maximumInclusive;
};


class RegExpFilter : public RoleFilter
{
    Q_OBJECT
    Q_PROPERTY(QString pattern READ pattern WRITE setPattern NOTIFY patternChanged)
    Q_PROPERTY(PatternSyntax syntax READ syntax WRITE setSyntax NOTIFY syntaxChanged)
    Q_PROPERTY(Qt::CaseSensitivity caseSensitivity READ caseSensitivity WRITE setCaseSensitivity NOTIFY caseSensitivityChanged)
    Q_ENUMS(PatternSyntax)

public:
    enum PatternSyntax {
        RegExp = QRegExp::RegExp,
        Wildcard = QRegExp::Wildcard,
        FixedString = QRegExp::FixedString,
        RegExp2 = QRegExp::RegExp2,
        WildcardUnix = QRegExp::WildcardUnix,
        W3CXmlSchema11 = QRegExp::W3CXmlSchema11
    };

public:
    explicit RegExpFilter(QObject *parent = nullptr);
    ~RegExpFilter() override {}

    QString pattern() const;
    void setPattern(const QString &pattern);

    PatternSyntax syntax() const;
    void setSyntax(PatternSyntax syntax);

    Qt::CaseSensitivity caseSensitivity() const;
    void setCaseSensitivity(Qt::CaseSensitivity caseSensitivity);

    bool filterRow(const QModelIndex &index) const override;

signals:
    void patternChanged();
    void syntaxChanged();
    void caseSensitivityChanged();

private:
    QRegExp m_regExp;
};


class AnyOf : public Filter, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit AnyOf(QObject *parent = nullptr);
    ~AnyOf() override {}

    bool filterRow(const QModelIndex &index) const override;

signals:
    void filtersChanged();

private:
    void onFilterAppended(Filter *filter) override;
};


class AllOf : public Filter, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit AllOf(QObject *parent = nullptr);
    ~AllOf() override {}

    bool filterRow(const QModelIndex &index) const override;

signals:
    void filtersChanged();

private:
    void onFilterAppended(Filter *filter) override;
};


class QQmlContext;
class QQmlExpression;
class ExpressionFilter : public Filter, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QQmlScriptString expression READ expression WRITE setExpression NOTIFY expressionChanged)

public:
    explicit ExpressionFilter(QObject *parent = nullptr);
    ~ExpressionFilter() override {}

    QQmlScriptString expression() const;
    void setExpression(const QQmlScriptString &scriptString);

    bool filterRow(const QModelIndex &index) const override;

signals:
    void expressionChanged();

protected:
    void classBegin() override {}
    void componentComplete() override;

private:
    void createContext();
    void evaluate();
    bool evaluate(QQmlExpression &expression) const;

private:
    QQmlScriptString m_scriptString;
    QQmlExpression* m_expression;
    QQmlContext* m_context;
};

/*
 * Use this to filter item by it childs
 * Each filter in ChildrenFilter applies to item's child
*/
class ChildrenFilter : public Filter, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit ChildrenFilter(QObject *parent = nullptr);
    ~ChildrenFilter() override {}

    bool filterRow(const QModelIndex &index) const override;

signals:
    void filtersChanged();

private:
    bool filterAcceptsRow(const QModelIndex &index) const;
    void onFilterAppended(Filter *filter) override;
};


class Contains : public RoleFilter, public Container
{
    Q_OBJECT
    Q_INTERFACES(Container)
    Q_PROPERTY(QQmlListProperty<Filter> filters READ filters NOTIFY filtersChanged)
    Q_CLASSINFO("DefaultProperty", "filters")

public:
    explicit Contains(QObject *parent = nullptr);
    ~Contains() override {}

    bool filterRow(const QModelIndex &index) const override;

signals:
    void filtersChanged();

private:
    bool filterAcceptsRow(const QModelIndex &index) const;
    void onFilterAppended(Filter *filter) override;
};


#endif // FILTER_H
